import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { fetchTeamRosters, initGamePlay, initNextPhase,
    initFight, startFight, processFightResults, doBeating, processPunch, processOvertimeBullyResults } from '../actions/GameActions';
import { throwDice, updateScoreBoard, updateGame } from '../actions/GamePlayActions';

import GameWrapper from '../components/game_components/Game/GameWrapper';

class Game extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {

    }

    componentWillUpdate() {

    }

    componentDidUpdate() {
        const { game, gamePlay, dispatch } = this.props;
        if((game.phase == 3 || game.phase == 6) && !game.beatingMode) {
            var fightResults = processFightResults(game, gamePlay);
            //znizenie atributov - asi priamo, nie cez bonusy
            dispatch(fightResults);
            if(fightResults.data.beatingMode){
                dispatch(doBeating());
            }
        }
    }

    processThrowDice() {
        const { game, gamePlay, dispatch } = this.props;
        var results = throwDice(game);
        dispatch(results);
        dispatch(updateGame(results.data));
        dispatch(updateScoreBoard(results.data.homeTeamScore,results.data.awayTeamScore));
        if(game.period>2) {
            if (Object.keys(gamePlay.overtimeBulyThrowResults[game.period]).length == 2) {
                dispatch(processOvertimeBullyResults(game, gamePlay));
            }
        }
    }

    initNextPhase() {
        const { game, dispatch } = this.props;
        //mozno by to bolo s bitkami lepsie tu, pretoze nextPhase este nevie vysledok fightResult
        //na druhu stranu, FightResults potrebuje updatnuty game, ktory tu este nie je..
        dispatch(initNextPhase(game));
    }

    initGamePlay() {
        const { game, dispatch} = this.props;
        dispatch(initGamePlay(game));
    }

    onPlayerCardClick(playerData,action) {
        console.log(action);
        console.log(playerData);
        switch(action){
            case 'fight' : this.initFight(playerData);
                break;
            case 'punch' : this.processPunch(playerData);
                break;
        }
    }

    initFight(playerData) {
        const { game, dispatch} = this.props;
        var fightState = initFight(game,playerData);
        dispatch(fightState);
        if(fightState.data.startFight){
            dispatch(startFight());
        }
    }

    processPunch(playerData) {
        const {game, dispatch} = this.props;
        dispatch(processPunch(game, playerData));
        dispatch(initNextPhase(game));
    }

    render() {
        const { game, gamePlay, dispatch } = this.props;

        return (<GameWrapper
            gameData={game}
            gamePlay={gamePlay}
            onHomeTeamJoin={(teamId) => dispatch(fetchTeamRosters(teamId,game.data.id,'home'))}
            onAwayTeamJoin={(teamId) => dispatch(fetchTeamRosters(teamId,game.data.id,'away'))}
            initGamePlay={() => this.initGamePlay()}
            onThrowDice={() => this.processThrowDice()}
            initNextPhase={() => this.initNextPhase()}
            onPlayerCardClick={(playerData,action) => this.onPlayerCardClick(playerData,action)}
        />)
    }
}

function select(state) {
    //console.log('Game state', state);
    return {
        game: state.game,
        gamePlay : state.gamePlay,
        userAuthSession: state.userAuthSession,
        userProfileData: state.userProfileData
    };
}

export default connect(select)(Game);