import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';

import TeamsList from '../components/game_components/TeamsList/TeamsList';
import { fetchTeamsList, teamSelected } from '../actions/TeamSelectActions.js';

import GamesList from '../components/game_components/GamesList/GamesList';
import { fetchGamesList, gameSelected } from '../actions/GameSelectActions.js';

import { initGame } from '../actions/GameActions';

import { initRosters } from '../actions/RostersActions';

class GameMenu extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.dispatch(fetchTeamsList());
        this.props.dispatch(fetchGamesList());
    }

    componentWillUpdate() {
        //this.props.dispatch(fetchTeamsList());
    }

    render() {
        var teamListContent = (<p>Pre zobrazenie timov je potrebné aby ste boli prihlasený</p>);
        const { dispatch, teamListData, gameListData, userProfileData } = this.props;

        console.log('userProfileData', userProfileData);
        console.log('selectedTeam', teamListData.selectedTeam);

        if (userProfileData !== undefined) {
            //if (userProfileData.userData !== null) {
            teamListContent = (
                <TeamsList
                    teamsListdata={teamListData.data}
                    onTeamSelected = { id =>
                                      {
                                        //dispatch({type:'server/hello'}),
                                        dispatch(teamSelected(id))
                                      }
                                     }
                 />
            );
            //}
        }

        console.log('isGameJoined', gameListData.isGameJoined);
        var gameListContent = (
          <GamesList
              gamesListData={gameListData.data}
              selectedTeam={teamListData.selectedTeam}
              onGameSelected={ id =>
                                {
                                    dispatch(gameSelected(id, teamListData.selectedTeam.id))
                                }
              }
              isGameJoined={gameListData.isGameJoined}
              isHomePlayerJoined={gameListData.isHomePlayerJoined}
              isAwayPlayerJoined={gameListData.isAwayPlayerJoined}
              onGameLaunch = {
                        (game) => {
                            browserHistory.push('/game');
                            dispatch(initGame(game))
                        }
                    }
              onSetRosters = {
                        (game) => {
                            browserHistory.push('/rosters');
                            dispatch(initRosters(game))
                        }
              }
              />
        );

        return (
            <div>
                <h1> Teams </h1>
                <div className="row">
                    <div className="col-xs-9">
                        { teamListContent }
                    </div>
                    <div className="col-xs-3">
                        { gameListContent }
                    </div>
                </div>
            </div>
        );
    }
}

GameMenu.contextTypes = {
    router: PropTypes.object.isRequired
};

function select(state) {
    console.log('Game state', state);
    return {
        userAuthSession: state.userAuthSession,
        teamListData : state.teamList,
        gameListData : state.gameList,
        userProfileData: state.userProfileData
    };
}

export default connect(select)(GameMenu);