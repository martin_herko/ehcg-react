import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { fetchTeamAvailPlayers, showAvailablePositionsForPlayer, assignPlayerPositionClick,
    fetchGameRosters, changeTeamRosterPeriod,removePlayerFromRosterPositionClick, teamRosterCompletedClick,
    requestGoaltenderDiceDraw} from '../actions/RostersActions';
import { initGame } from '../actions/GameActions';

import RosterGameField from '../components/game_components/GameRosters/RosterGameField';
import RosterPlayersListbox from '../components/game_components/GameRosters/RosterPlayersListbox';

import localStorage from 'store';

class GameRosters extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {

    }

    componentDidMount() {
        const { rosters , dispatch } = this.props;
        console.log('mount',rosters);
        dispatch(fetchTeamAvailPlayers(rosters.gameData.home_team, rosters.gameData.id));
        dispatch(fetchTeamAvailPlayers(rosters.gameData.away_team, rosters.gameData.id));
        dispatch(fetchGameRosters(rosters.gameData.id));
    }

    componentWillUpdate() {

    }

    componentDidUpdate() {
        const { rosters , dispatch } = this.props;
        console.log('componentWillUpdate', rosters);
        if(rosters.homeTeamRosterCompleted && rosters.awayTeamRosterCompleted){
            browserHistory.push('/game');
            dispatch(initGame(rosters.gameData))
        }
    }

    onPlayerOptionclick(playerData,role) {
        const { dispatch } = this.props;
        dispatch(showAvailablePositionsForPlayer(playerData,role));
    }

    onAssignPlayerPositionClick(playerData,position) {
        const { rosters, dispatch } = this.props;
        console.log('onAssignPlayerPositionClick',playerData);
        dispatch(assignPlayerPositionClick(rosters, playerData, position));
    }

    onTeamPeriodChange(role, event){
        const { rosters, dispatch } = this.props;
        dispatch(changeTeamRosterPeriod(event.target.value,role));
        dispatch(fetchGameRosters(rosters.gameData.id));
    }

    onRemovePlayerFromPositionClick(playerData) {
        const { rosters, dispatch } = this.props;
        dispatch(removePlayerFromRosterPositionClick(playerData));
    }

    onTeamRosterCompletedClick(role) {
        const { dispatch, rosters } = this.props;
        let roster = role=='home' ? rosters.homeTeamRoster : rosters.awayTeamRoster;
        dispatch(teamRosterCompletedClick(role, roster, rosters.gameData));
    }

    onGoaltenderDiceDraw(gameId, teamId) {
        const { dispatch } = this.props;
        dispatch(requestGoaltenderDiceDraw(gameId, teamId));
    }

    render() {
        const { rosters, teamList } = this.props;
        console.log(rosters);
        console.log(teamList);
        var homeTeamPlayerList = rosters.homeTeamPlayerListLoaded ? rosters.homeTeamPlayerList : false;
        var awayTeamPlayerList = rosters.awayTeamPlayerListLoaded ? rosters.awayTeamPlayerList : false;
        var homeGameRosters = rosters.homeTeamRoster !==null ? rosters.homeTeamRoster[rosters.homeTeamPeriod] : null;
        var awayGameRosters = rosters.awayTeamRoster !==null ? rosters.awayTeamRoster[rosters.awayTeamPeriod] : null;
        var teamsList = localStorage.get('teamsList');
        console.log('teamsList',teamsList);
        var isUserHomeTeam = teamList.selectedTeam.id == rosters.gameData.home_team;
        var isUserAwayTeam = teamList.selectedTeam.id == rosters.gameData.away_team;

        if(isUserHomeTeam){
            var homeRosterConfirmButton = <button onClick={()=>this.onTeamRosterCompletedClick('home')} disabled={rosters.homeTeamRosterCompleted ? "disabled":""}>{rosters.homeTeamRosterCompleted ? "Potvrdené ..":"Potvrdiť zostavu"}</button>;
            var awayRosterConfirmButton = <span>Čaká sa na potvrdenie od súpera</span>
        }

        if(isUserAwayTeam){
            var homeRosterConfirmButton = <span>Čaká sa na potvrdenie od súpera</span>
            var awayRosterConfirmButton = <button onClick={()=>this.onTeamRosterCompletedClick('away')} disabled={rosters.awayTeamRosterCompleted ? "disabled":""}>{rosters.awayTeamRosterCompleted ? "Potvrdené ..":"Potvrdiť zostavu"}</button>;
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-3"><h4>Rosters</h4></div>
                </div>
                <div className="row">
                    <div className="col-xs-4" id="homeTeamLogo">
                        <img className="img responsive" src={'/images/teams/' + teamsList[rosters.gameData.home_team].code + '.gif'} height="50"/>
                    </div>
                    <div className="col-xs-4">
                        <span>Line </span>
                        <select id="homeTeamPeriodSelect" onChange={this.onTeamPeriodChange.bind(this, 'home')}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div className="col-xs-4">{homeRosterConfirmButton}</div>
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <RosterGameField role={'home'}
                                         showAvailablePositions={rosters.homeTeamPlayerShowPlayerAvailablePositions}
                                         selectedPlayerData={rosters.homePlayerSelectedData}
                                         onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                         onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                         gamePeriodRosters={homeGameRosters}
                                         hasUserControl={isUserHomeTeam}
                            />
                    </div>
                    <div className="col-xs-4">
                        <RosterPlayersListbox role={'home'}
                                              playerList={homeTeamPlayerList}
                                              selectedPlayerData={rosters.homePlayerSelectedData}
                                              onPlayerOptionClick={(playerData,role)=>this.onPlayerOptionclick(playerData,role)}
                                              onGoaltenderDiceDraw={()=>this.onGoaltenderDiceDraw(rosters.gameData.id,rosters.gameData.home_team)}
                                              assignedGoaltenderData={rosters.homeTeamGoaltenderAssignedData}
                                              hasUserControl={isUserHomeTeam}
                            />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4" id="awayTeamLogo">
                        <img className="img responsive" src={'/images/teams/' + teamsList[rosters.gameData.away_team].code + '.gif'} height="50"/>
                    </div>
                    <div className="col-xs-4">
                        <span>Line </span>
                        <select id="homeTeamPeriodSelect" onChange={this.onTeamPeriodChange.bind(this, 'away')}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div className="col-xs-4">{awayRosterConfirmButton}</div>
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <RosterGameField role={'away'}
                                         showAvailablePositions={rosters.awayTeamPlayerShowPlayerAvailablePositions}
                                         selectedPlayerData={rosters.awayPlayerSelectedData}
                                         onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                         onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                         gamePeriodRosters={awayGameRosters}
                                         hasUserControl={isUserAwayTeam}
                            />
                    </div>
                    <div className="col-xs-4">
                        <RosterPlayersListbox role={'away'}
                                              playerList={awayTeamPlayerList}
                                              selectedPlayerData={rosters.awayPlayerSelectedData}
                                              onPlayerOptionClick={(playerData,role)=>this.onPlayerOptionclick(playerData,role)}
                                              onGoaltenderDiceDraw={()=>this.onGoaltenderDiceDraw(rosters.gameData.id,rosters.gameData.away_team)}
                                              assignedGoaltenderData={rosters.awayTeamGoaltenderAssignedData}
                                              hasUserControl={isUserAwayTeam}
                            />
                    </div>
                </div>
            </div>
        );
    }
}

GameRosters.contextTypes = {
    router: PropTypes.object.isRequired
};

function select(state) {
    console.log('Game state', state);
    return {
        rosters: state.rosters,
        teamList: state.teamList,
        userAuthSession: state.userAuthSession,
        userProfileData: state.userProfileData
    };
}

export default connect(select)(GameRosters);