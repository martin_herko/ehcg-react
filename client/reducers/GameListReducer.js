import {  Start_Fetching_GamesList,
    Fetch_GamesList_Success, Fetch_GamesList_Fail,
    Reload_GamesList_Page,
    Game_Selected
    } from '../actions/GameSelectActions';

import { Join_To_Game_Success } from '../actions/GameActions.js';

const defaultGamesListState = {
    listLoaded : false,
    isFetchingList : false,
    isGameJoined : false,
    isHomePlayerJoined : false,
    isAwayPlayerJoined : false,
    data : {},
    error : null
}

export function updateGameList(gameListState = defaultGamesListState , action = null) {

    switch (action.type){
        case Start_Fetching_GamesList:
            return Object.assign({}, defaultGamesListState,{isFetchingList: true });

        case Fetch_GamesList_Success:
            return {
                listLoaded: true,
                isFetchingList: false,
                isGameJoined : false,
                data: action.gamesList,
                error: null
            };


        case Fetch_GamesList_Fail:
            return {
                listLoaded: true,
                isFetchingList: false,
                isGameJoined : false,
                data: null,
                error: action.error
            };

        case Reload_GamesList_Page:
            return Object.assign({}, defaultGamesListState);

        case Game_Selected:
            return {
                listLoaded: true,
                isFetchingList: false,
                isGameJoined : false,
                data: {0:action.game},
                error: action.error
            };

        case Join_To_Game_Success:
            console.log('joinToGameSuccess');
            var isHomePlayerJoined = action.data.joinedPlayersCount >= 1 ? true : gameListState.isHomePlayerJoined;
            var isAwayPlayerJoined = action.data.joinedPlayersCount == 2 ? true : gameListState.isAwayPlayerJoined;

            return {
                listLoaded: true,
                isFetchingList: false,
                isGameJoined : true,
                isHomePlayerJoined : isHomePlayerJoined,
                isAwayPlayerJoined : isAwayPlayerJoined,
                data: {0: action.data.game},
                error: action.error
            };

        default:
            return gameListState;
    }
}