import {    Join_To_Game_SUCESS,
            Init_Game, Team_Roster_Loaded, Init_FightPhases, InitFight, Process_Fight_Result, PunchProcessed,
            Init_Next_Phase, Process_Overtime_Buly_Result, End_Game
    } from '../actions/GameActions';

import { Update_Scoreboard, Update_Game, getInitBonuses} from '../actions/GamePlayActions';

export const defaultGameState = {
    isGameJoined : false,
    homePlayer : 1,
    awayPlayer : 3,
    homeScore : 0,
    awayScore : 0,
    period : 1,
    phase : 0,
    activeTeam : 1,
    /*activeAssister : 1,
    activeShooter : 2,
    activeGoalTender : 5,*/
    action : '',
    data : {},
    //ppTeam1 : false, //powerplay first team
    //ppTeam2 : false,
    draw : false,
    // ppData : {period : {ft,...}}
    ppData : {3:{'firstTeam':0,'firstScored':false},4:{'firstTeam':0,'firstScored':false},5:{'firstTeam':0,'firstScored':false}},
    // teamdId : {period : {action : {value}}}
    bonuses : {},
    /*homePlayerRoster : {},
    awayPlayerRoster : {},*/
    fighters : {},
    fightPeriod : 1,
    beatingMode : false,
    homeFighter : false,
    awayFighter : false,
    error : null
}

export function updateGame(gameState = defaultGameState , action = null) {

    switch (action.type){
        case Init_Game:
            var initBonuses = getInitBonuses(action.game.home_team,action.game.away_team);
            return Object.assign({}, gameState, {
                isGameJoined : true,
                homePlayer : action.game.home_team,
                awayPlayer : action.game.away_team,
                phase : action.game.phase,
                activeTeam : action.game.home_team,
                action : 'gameplayInit',
                data : action.game,
                bonuses : initBonuses
            });

        case Team_Roster_Loaded :
            if(action.data.role=='home') {
                return Object.assign({}, gameState,{
                    homePlayerRoster : action.data.roster
                });
            }
            if(action.data.role=='away') {
                return Object.assign({}, gameState,{
                    awayPlayerRoster : action.data.roster
                });
            }

        case Init_Next_Phase :
            return Object.assign({}, gameState, {
                phase : action.data.phase,
                period : action.data.period,
                activeTeam : action.data.activeTeam,
                activeAssister : action.data.activeAssister,
                activeShooter : action.data.activeShooter,
                activeGoalTender : action.data.activeGoalTender,
                fightPeriod : action.data.fightPeriod,
                action: action.data.action
            });

        case Init_FightPhases :
            return Object.assign({}, gameState, {
                fighters : action.data.fighters,
                action : 'fight',
                activeAssister : null,
                activeShooter : null,
                activeGoalTender : null
            });

        case InitFight :
            return Object.assign({},gameState, {
                phase : action.data.phase,
                action : 'fight',
                homeFighter: action.data.homeFighter,
                awayFighter: action.data.awayFighter,
                fighters: action.data.fighters
            });

        case Process_Fight_Result :
            var activeFighter = action.data.ppTeam1 == gameState.homePlayer ? gameState.homeFighter : gameState.awayFighter;

            return Object.assign({},gameState, {
                draw : action.data.draw,
                beatingMode : action.data.beatingMode,
                ppTeam1 : action.data.ppTeam1,
                ppTeam2 : action.data.ppTeam2,
                activeTeam : action.data.ppTeam1,
                activeFighter : activeFighter.index
            });

        case PunchProcessed :
            return Object.assign({},gameState, {
                beatingMode : false,
                homePlayerRoster: action.data.homePlayerRoster,
                awayPlayerRoster: action.data.awayPlayerRoster,
                fighters : action.data.fighters,
                ppTeam1 : 'na',
                ppTeam2 : 'na',
                activeFigher : false
            });

        case Process_Overtime_Buly_Result :
            let ppDataFTUpdate = gameState.ppData;
            ppDataFTUpdate[gameState.period]['firstTeam'] = action.data.ppTeam1;
            return Object.assign({}, gameState, {
                ppTeam1 : action.data.ppTeam1,
                ppTeam2 : action.data.ppTeam2,
                draw : action.data.draw,
                ppData : ppDataFTUpdate
            });

        case Update_Game :
            let ppDataFSUpdate = gameState.ppData;
            if(action.data.ppFirstTeamScored!='undefined'){
                ppDataFSUpdate[gameState.period]['firstScored'] = action.data.ppFirstTeamScored;
            }
            return Object.assign({}, gameState, {
                bonuses : action.data.bonuses,
                ppData : ppDataFSUpdate
            });

        case Update_Scoreboard :
            return Object.assign({}, gameState, {
                homeScore : action.data.homeScore,
                awayScore : action.data.awayScore
            });

        case End_Game :
            return Object.assign({}, gameState, {
            phase : action.data.phase
        });

        default:
            return gameState;
    }
}