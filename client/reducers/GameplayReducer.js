import { StartFight, DoBeating, Init_Next_Phase } from '../actions/GameActions';
import { Throw_dice_result } from '../actions/GamePlayActions';

const defaultGameState = {
    diceThrowResults : [],
    diceThrowResult : false,
    overtimeBulyThrowResults : {3:{},4:{},5:{}}, //periods 3-4-5
    fightThrowResults : {1:{},2:{},4:{},5:{}}, //phases 1,2,4,5 3,6-beating
    diceThrowEnabled : false
}


export function gamePlay(gameState = defaultGameState, action = null) {
    console.log('updateGame action ', action);

    switch (action.type) {
        case Throw_dice_result:
        {
            let results = gameState.diceThrowResults;
            results.push(action.data);
            switch(action.data.action){
                case 'buly':
                {
                    let overtimeBulyResults = gameState.overtimeBulyThrowResults;
                    overtimeBulyResults[action.data.period][action.data.activeTeam] = action.data.diceResult;

                    return Object.assign({}, gameState, {
                        diceThrowResults: results,
                        diceThrowResult: action.data,
                        diceThrowEnabled: false,
                        overtimeBulyThrowResults: overtimeBulyResults
                    });
                }
                case 'fight' :
                {
                    let fightThrowResults = gameState.fightThrowResults;
                    fightThrowResults[action.data.phase][action.data.activeTeam] = {
                        absolute : action.data.resultAbsolute,
                        diceResult : action.data.diceResult
                    };

                    return Object.assign({}, gameState, {
                        diceThrowResults: results,
                        diceThrowResult: action.data,
                        diceThrowEnabled: false,
                        fightThrowResults : fightThrowResults
                    });
                }
                default: return Object.assign({}, gameState, {
                    diceThrowResults: results,
                    diceThrowResult: action.data,
                    diceThrowEnabled: false
                });
            }

        }

        case StartFight:
            return Object.assign({}, gameState, {
                diceThrowResult : false,
                diceThrowEnabled : true
            });

        case DoBeating:
            return Object.assign({}, gameState, {
                diceThrowEnabled : false
            });

        case Init_Next_Phase:
            return Object.assign({}, gameState,{
                //diceThrowResults : [],
                diceThrowResult : false,
                diceThrowEnabled : true
            });

        default:
            return gameState;
    }
}