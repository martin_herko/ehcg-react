import {  Init_Rosters, Team_Player_List_Loaded, Show_Available_Positions_For_Player, AssignGoaltenderByDiceDraw,  GameRostersUpdate, ChangeTeamRosterPeriod, TeamRosterCompleted
    } from '../actions/RostersActions.js';
import {Game_Update} from '../actions/GameActions.js';

const defaultStartState = {
    rostersInited:false,
    homeTeamPlayerListLoaded: false, //true even if results failed
    homeTeamPlayerList: null,
    homeTeamPlayerShowPlayerAvailablePositions: false,
    homeTeamRosterCompleted: false,
    homeTeamGoaltenderAssignedData: false,
    homePlayerSelectedData: null,
    homeTeamRoster: null,
    awayTeamPlayerListLoaded: false, //true even if results failed
    awayTeamPlayerList: null,
    awayTeamRosterCompleted: false,
    awayTeamPlayerShowPlayerAvailablePositions: false,
    awayTeamGoaltenderAssignedData: false,
    awayPlayerSelectedData: null,
    awayTeamRoster: null,
    gameData : null,
    error: null,
    homeTeamPeriod: 1,
    awayTeamPeriod: 1
}

export function rosters(rostersState = defaultStartState , action = null) {
    switch (action.type){
        case Init_Rosters:
            return Object.assign({}, rostersState,{
                rostersInited: true,
                gameData: action.data,
                homeTeamRosterCompleted: false,
                awayTeamRosterCompleted: false
            });
        case Team_Player_List_Loaded:
            console.log('teamPlayerList',action.teamList);
            if(rostersState.rostersInited) {
                var role = action.teamId == rostersState.gameData.home_team ? 'home' : 'away';
                switch (role) {
                    case 'home' :
                        return Object.assign({}, rostersState, {
                            homeTeamPlayerListLoaded: true,
                            homeTeamPlayerList: action.teamList
                        });
                    case 'away' :
                        return Object.assign({}, rostersState, {
                            awayTeamPlayerListLoaded: true,
                            awayTeamPlayerList: action.teamList
                        });
                }
            }else{
                return rostersState;
            }
        case Show_Available_Positions_For_Player:
            switch(action.role) {
                case 'home' :
                return Object.assign({}, rostersState, {
                    homeTeamPlayerShowPlayerAvailablePositions : true,
                    homePlayerSelectedData:action.playerData
                });
                case 'away' :
                return Object.assign({}, rostersState, {
                    awayTeamPlayerShowPlayerAvailablePositions : true,
                    awayPlayerSelectedData:action.playerData
                });
            }
        case AssignGoaltenderByDiceDraw:
            console.log('Reducer - AssignGoaltenderByDiceDraw', action.data);
            var role = action.data.team_id==rostersState.gameData.home_team ? 'home' : 'away';
            console.log('Reducer - AssignGoaltenderByDiceDraw-ROLE',role);
            switch(role)
            {
                case 'home' :
                    return Object.assign({}, rostersState, {
                        homeTeamGoaltenderAssignedData: action.data
                    });
                case 'away' :
                    return Object.assign({}, rostersState, {
                        awayTeamGoaltenderAssignedData :action.data
                    });
            }
        case GameRostersUpdate:
            console.log('Reducer - GameRostersUpdate',action.data);
            if(typeof(action.data)!=='undefined' && rostersState.rostersInited){
                return Object.assign({}, rostersState, {
                    awayTeamRoster : typeof(action.data[rostersState.gameData.away_team])!=='undefined' ? action.data[rostersState.gameData.away_team] : null,
                    homeTeamRoster : typeof(action.data[rostersState.gameData.home_team])!=='undefined' ? action.data[rostersState.gameData.home_team] : null,
                    homeTeamPlayerShowPlayerAvailablePositions : false,
                    awayTeamPlayerShowPlayerAvailablePositions : false
                });
            } else return rostersState;
        case ChangeTeamRosterPeriod:
            switch(action.role){
                case 'home' : return Object.assign({}, rostersState,{
                    homeTeamPeriod:parseInt(action.period)
                });
                case 'away' : return Object.assign({}, rostersState,{
                    awayTeamPeriod:parseInt(action.period)
                });
            }
        case TeamRosterCompleted :
            switch(action.role){
                case 'home' : return Object.assign({}, rostersState,{
                    homeTeamRosterCompleted: action.result
                });
                case 'away' : return Object.assign({}, rostersState,{
                    awayTeamRosterCompleted: action.result
                });
            }
        case Game_Update :
            console.log('Game_Update',action.game);
            return Object.assign({}, rostersState, {
                gameData: action.game,
                homeTeamRosterCompleted: action.game.home_team_roster_completed,
                awayTeamRosterCompleted: action.game.away_team_roster_completed
            });
        default:
            return rostersState;
    }
}