import {  Start_Fetching_TeamsList,
    Fetch_TeamsList_Success, Fetch_TeamsList_Fail,
    Reload_TeamsList_Page,
    Team_Selected
    } from '../actions/TeamSelectActions';

const defaultTeamsListState = {
    listLoaded : false,
    isFetchingList : false,
    data : {1:'loading'},
    selectedTeam : false,
    error : null
}

export function updateTeamList(teamListState = defaultTeamsListState , action = null) {

    switch (action.type){
        case Start_Fetching_TeamsList:
            return Object.assign({}, defaultTeamsListState,{isFetchingList: true });

        case Fetch_TeamsList_Success:
            console.log('TeamSelectReducer action ',action);
            return {
                listLoaded: true,
                isFetchingList: false,
                data: action.teamsList,
                selectedTeam : false,
                error: null
            };


        case Fetch_TeamsList_Fail:
            return {
                listLoaded: true,
                isFetchingList: false,
                data: null,
                selectedTeam : false,
                error: action.error
            };

        case Reload_TeamsList_Page:
            return Object.assign({}, defaultTeamsListState);

        case Team_Selected:
            console.log(action);
            return {
                listLoaded: true,
                isFetchingList: false,
                data: {0:action.team},
                selectedTeam : action.team,
                error: action.error
            };

        default:
            return teamListState;
    }
}