//HD1A - Home Defense 1 assist, ...

export const GamePhases = [
    'gameplayInit',
    'F1H1FF','F1A1FF', //Fight 1, Home 1st fight throw
    'F1WFP',             //Fight 1, Winner is beating/punch
    'F2H2FF','F2A2FF',
    'F2WFP',
    'P1HD1A','P1HLWS',
    'P1AD1A','P1ALWS',
    'P1HLWA','P1HCTS',
    'P1ALWA','P1ACTS',
    'P1HCTA','P1HRWS',
    'P1ACTA','P1ARWS',
    'P1HRWA','P1HD2S',
    'P1ARWA','P1AD2S',
    'P1HD2A','P1HD1S',
    'P1AD2A','P1AD1S',
    'P2HD1A','P2HLWS',
    'P2AD1A','P2ALWS',
    'P2HD2A','P2HCTS',
    'P2AD2A','P2ACTS',
    'P2HRWA','P2HRWS',
    'P2ARWA','P2ARWS',
    'P2HCTA','P2HD2S',
    'P2ACTA','P2AD2S',
    'P2HD2A','P2HD1S',
    'P2AD2A','P2AD1S',
    'O3H1DB','O3A1DB', //Overtime 1 Home 1st Dice Buly
    'O3WLWA','03WCTS', //overtime 1, winner lw assist
    'O3LD1A','O3LD2S',
    'O4H1DB','O4A1DB',
    'O4WLWA','04WCTS', //overtime 2, winner lw assist
    'O4LD1A','O4LD2S',
    'O5H1DB','O5A1DB',
    'O5WLWA','O5WCTS',
    'O5LD1A','O5LD2S',
    'End_Game'
];
