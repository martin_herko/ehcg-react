//Cache
import localStorage from 'store';

export const Start_Fetching_TeamsList = 'Start_Fetching_TeamsList';
export const Fetch_TeamsList_Success = 'Fetch_TeamsList_Success';
export const Fetch_TeamsList_Fail = 'Fetch_TeamsList_Fail';
export const Reload_TeamsList_Page = 'Reload_TeamsList_Page';
export const Team_Selected = 'Team_Selected';

export function startFetchingTeamsList() {
    return { type: Start_Fetching_TeamsList };
}


export function fetchTeamsList() {
    return {type:'server/teamsList'};

    return (dispatch) => {
        dispatch(startFetchingTeamsList());

        /*$.ajax({
            type: 'GET',
            url: ('/api/v1/getTeams') })
            .done(function(data) {
                console.log('fetchTeamsList() data',data);
                if (data.error){
                    dispatch(fetchTeamsListFail(data.error));
                } else {
                    dispatch(fetchTeamsListSuccess(data.teams));
                }
            })
            .fail(function(a,b,c,d) {
                console.log("GET '/api/v1/getTeams/' has actual failure: ", a, b, c, d)
                dispatch(fetchTeamsListFail()); //TODO figure out what to pass
            });*/
    }
}

export function teamSelected(team) {
    return {type : Team_Selected, team};
}

export function fetchTeamsListSuccess(teamsList) {
    cacheTeamsList(teamsList);
    return { type: Fetch_TeamsList_Success, teamsList };
}

export function fetchTeamsListFail(error) {
    return { type: Fetch_TeamsList_Fail, error };
}

export function reloadingTeamsListPage() {
    return { type: Reload_TeamsList_Page };
}

export function cacheTeamsList(teamsList)
{
    console.log('teamsList',teamsList);
    var teams = {};
    // see : http://www.2ality.com/2013/05/quirk-array-like-objects.html
    Array.prototype.forEach.call(teamsList, team => {teams[team.id] = team});

    localStorage.set('teamsList', teams);
}


