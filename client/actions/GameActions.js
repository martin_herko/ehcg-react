export const Join_To_Game_Success = 'Join_To_Game_Success';
export const Init_Game = 'Init_Game';
export const Game_Update = 'Game_Update';
export const Fetch_Team_Roster = 'Fetch_Team_Roster';
export const Team_Roster_Loaded = 'Team_Roster_Loaded';
export const Init_Next_Phase = 'Init_Next_Phase';
export const Process_Overtime_Buly_Result = 'Process_Overtime_Buly_Result';
export const End_Game = 'End_Game';
export const Init_FightPhases = 'Init_FightPhases';
export const InitFight = 'InitFight';
export const StartFight = 'StartFight';
export const DoBeating = 'doBeating';
export const PunchProcessed = 'PunchProcessed';
export const Process_Fight_Result = 'Process_Fight_Result';

import { GamePhases } from './Constants/GamePhases';

export function joinToGameSuccess(data) {
    return { type: Join_To_Game_Success, data:{game:data.game, joinedPlayersCount:data.joinedPlayersCount} };
}

export function initGame(game) {
    return { type: Init_Game, game}
}

export function gameUpdate(game) {
    return { type: Game_Update, game}
}

export function fetchTeamRosters(teamId, gameId, role) {
    return {type:'server/teamRoster', teamId:teamId, gameId:gameId, role:role};
}

export function teamRosterLoaded(roster, role) {
    return { type: Team_Roster_Loaded, data : {roster:roster, role:role}};
}

export function initGamePlay(game) {
    return checkIfFightsGonaHappens(game);
}

function checkIfFightsGonaHappens(game) {
    //Check if fights gona happens..
    let homePlayer = game.homePlayer;
    let awayPlayer = game.awayPlayer;
    let fighters = {};
    fighters[homePlayer] = {1: {}, 2: {}};
    fighters[awayPlayer] = {1: {}, 2: {}};
    let linesToConsider = [1, 2];
    let doFights = false;
    let defaultFighterParams = {activated: false, fightResult: false, beatedPlayer: false};

    //Home Team
    for (var line in linesToConsider) {
        for (var player in game.homePlayerRoster[line]) {
            if (game.homePlayerRoster[line][player].fight > 0) {
                let id = game.homePlayerRoster[line][player].id;
                fighters[homePlayer][line][id] = defaultFighterParams;
                doFights = true;
            }
        }

        for (var player in game.awayPlayerRoster[line]) {
            if (game.awayPlayerRoster[line][player].fight > 0) {
                let id = game.awayPlayerRoster[line][player].id;
                fighters[awayPlayer][line][id] = defaultFighterParams;
                doFights = true;
            }
        }
    }

    if (doFights) {
        return {type: Init_FightPhases, data: {fighters: fighters}};
    } else {
        return skipAllFightPhases(game);
    }
}

function checkIfTeamHaveAvailableFighter(game, role) {
    let fighters = role == 'home' ? game.fighters[game.homePlayer] : game.fighters[game.awayPlayer];
    var result = false;

    for (var player in fighters[game.fightPeriod]){
        if(fighters[game.fightPeriod][player].activated==false){
            result = true;
        }
    }

    console.log('checkIfTeamHaveAvailableFighter',result);
    return result;
}

function skipAllFightPhases(game) {
    for(var phase in GamePhases){
        if(GamePhases[game.phase].substr(0,1)=='F'){
            game.phase++;
        }
    }

    //Because initNextPhase will increment phase anyway..
    game.phase -=1;
    console.log('No fights gona happend, skipping to first period..');
    return initNextPhase(game);
}

export function initFight(game,playerData) {
    let role = playerData.team_id == game.homePlayer ? 'home' : 'away';
    let homeFighter = false;
    let awayFighter = false;

    if(role == 'home'){
        homeFighter = playerData;
        // if away player is not already set
        if(!game.awayFighter) {
            awayFighter = checkIfTeamHaveAvailableFighter(game, 'away') ? 'expected' : 'na';
        } else {
            awayFighter = game.awayFighter;
        }
    } else {
        if(!game.homeFighter){
            homeFighter = checkIfTeamHaveAvailableFighter(game, 'home') ? 'expected' : 'na';
        } else {
            homeFighter = game.homeFighter;
        }
        awayFighter = playerData;
    }

    let nextPhase = getNextPhase(game);
    let startFight = true;
    //if one from fighters is still expected to be set by player interaction, hold the phase
    if(homeFighter=='expected' || awayFighter=='expected'){
        nextPhase = game.phase;
        startFight = false;
    }

    var fighters = game.fighters;
    fighters[playerData.team_id][game.fightPeriod][playerData.id] = {activated :true, fightResult: false, beatedPlayer:false};

    return { type: InitFight, data:{phase:nextPhase, fighters : fighters, homeFighter:homeFighter, awayFighter:awayFighter, startFight : startFight}};
}

export function startFight() {
    return { type: StartFight };
}

export function initNextPhase(game) {
    var nextPhase = getNextPhase(game);
    if(GamePhases[nextPhase] == End_Game || GamePhases[game.phase] == End_Game){
        return endGame();
    }else{
        var nextPeriod = getNextPeriod(nextPhase,game);
        var activeTeam = getNextActiveTeam(nextPhase,game);
        var activeAssister = getNextActiveAssister(nextPhase,game);
        var activeShooter = getNextActiveShooter(nextPhase,game);
        var activeGoalTender = getNextActiveGoalTender(nextPhase,game);
        var nextFightPeriod = getNextFightPeriod(game);
        var action = getNextAction(nextPhase,game);
        var fightPeriod = getNextFightPeriod(nextPhase,game);

        return { type: Init_Next_Phase,
            data: {phase:nextPhase,period:nextPeriod,activeTeam:activeTeam,activeAssister:activeAssister,
                activeShooter:activeShooter,activeGoalTender:activeGoalTender,
                fightPeriod:nextFightPeriod, fightPeriod:fightPeriod, action:action} };
    }
}

export function getNextPhase(game){
    let nextPhase = game.phase + 1;

    //if fight draw result
    if(GamePhases[game.phase] == 'F1A1FF' || GamePhases[game.phase] == 'F2A2FF'){
        if(game.draw){
            console.log('Fight draw result detected, there will be no fight..');
            return nextPhase += 2;
        }
    }

    //if first fight round ended, check if next fight round gona even happend..
    if(GamePhases[game.phase] == 'F1WFP'){
        if((Object.keys(game.fighters[game.homePlayer][2]).length === 0) && (Object.keys(game.fighters[game.awayPlayer][2]).length === 0)){
            return nextPhase +=3;
        }
    }

    //if buly and draw result
    if(GamePhases[game.phase] == 'O3A1DB' || GamePhases[game.phase] == 'O4A1DB' || GamePhases[game.phase] == 'O5A1DB'){
        if(game.draw){
            console.log('Overtime buly draw result detected, repeating..');
            return getPreviousPhase(game);
        }
    }

    //Check if "oslabovka" gonna even happend :) Only IF ppTeam1 doesnt scored
    if(GamePhases[nextPhase]=='O3LD1A' || GamePhases[nextPhase]=='O4LD1A' || GamePhases[nextPhase]=='O5LD1A'){
        if(game.ppData[game.period]['firstScored']){
            nextPhase += 2;
        }
    }
    //Check if period 5 gonna even happend :) Only IF ppTeam1-O1 != ppTeam1-O2
    if(GamePhases[nextPhase]=='O5H1DF'){
        if(game.ppData[3]['firstTeam'] == game.ppData[4]['firstTeam']){
            nextPhase += 6;
        }
    }

    return nextPhase;
}

export function getPreviousPhase(game){
    return game.phase - 1;
}

export function processFightResults(game, gamePlay) {
    var fphase1 = 1;
    var fphase2 = 2;

    switch(GamePhases[game.phase]){
        case 'F1WFP' : {

        } break;
        case 'F2WFP' : {
            fphase1 = 4;
            fphase2 = 5;
        }
    }

    console.log('processFightResults',fphase1, fphase2);

    let homePlayerResult = gamePlay.fightThrowResults[fphase1][game.homePlayer];
    let awayPlayerResult = gamePlay.fightThrowResults[fphase2][game.awayPlayer];

    let homePlayerDouble = homePlayerResult.diceResult.firstDice == homePlayerResult.diceResult.secondDice;
    let awayPlayerDouble = awayPlayerResult.diceResult.firstDice == awayPlayerResult.diceResult.secondDice;;

    //If player throw two identical numbers, it means win !
    if(homePlayerDouble && !awayPlayerDouble){
        return { type : Process_Fight_Result, data : {draw:false, beatingMode : true, ppTeam1:game.homePlayer,
            ppTeam2:game.awayPlayer, activeFighter:game.homeFighter}};
    }

    if(!homePlayerDouble && awayPlayerDouble){
        return { type : Process_Fight_Result, data : {draw:false, beatingMode : true, ppTeam1:game.awayPlayer,
            ppTeam2:game.homePlayer, activeFighter:game.awayPlayer}};
    }

    if(homePlayerResult.absolute == awayPlayerResult.absolute){
        return { type : Process_Fight_Result, data : {draw:true, beatingMode : false}};
    }

    if(homePlayerResult.absolute > awayPlayerResult.absolute){
        return { type : Process_Fight_Result, data : {draw:false, beatingMode : true, ppTeam1:game.homePlayer,
            ppTeam2:game.awayPlayer, activeFighter:game.homeFighter}};
    } else {
        return { type : Process_Fight_Result, data : {draw:false, beatingMode : true, ppTeam1:game.awayPlayer,
            ppTeam2:game.homePlayer, activeFighter:game.awayPlayer}};
    }
}

export function doBeating() {
    return {type : DoBeating};
}

export function processPunch(game, playerData) {
    if(game.ppTeam1 == game.homePlayer){
        let period = game.homeFighter.game_period;
        if (game.awayPlayerRoster[period][playerData.index].atr1 >0) {
            game.awayPlayerRoster[period][playerData.index].atr1 = 0;
        }
        if (game.awayPlayerRoster[period][playerData.index].atr2 >0) {
            game.awayPlayerRoster[period][playerData.index].atr2 = 0;
        }
    }
    if(game.ppTeam1 == game.awayPlayer){
        let period = game.awayFighter.game_period;
        if (game.homePlayerRoster[period][playerData.index].atr1 >0) {
            game.homePlayerRoster[period][playerData.index].atr1 = 0;
        }
        if (game.homePlayerRoster[period][playerData.index].atr2 >0) {
            game.homePlayerRoster[period][playerData.index].atr2 = 0;
        }
    }

    let fighters = game.fighters;
    let activeFighterId = game.ppTeam1 == game.homePlayer ? game.homeFighter.id:game.awayFighter.id
    fighters[game.ppTeam1][game.fightPeriod][activeFighterId] = {activated : true, beatedPlayer:playerData, fightResult:'win'};

    return {
        type : PunchProcessed, data : {homePlayerRoster:game.homePlayerRoster, awayPlayerRoster : game.awayPlayerRoster, fighters:fighters}
    }
}

export function processOvertimeBullyResults(game, gamePlay) {
    if(gamePlay.overtimeBulyThrowResults[game.period][game.homePlayer] == gamePlay.overtimeBulyThrowResults[game.period][game.awayPlayer]){
        return { type: Process_Overtime_Buly_Result, data : {draw:true}}
    }

    if(gamePlay.overtimeBulyThrowResults[game.period][game.homePlayer]>gamePlay.overtimeBulyThrowResults[game.period][game.awayPlayer]){
        return { type: Process_Overtime_Buly_Result, data : {draw:false,ppTeam1:game.homePlayer,ppTeam2:game.awayPlayer}}
    } else {
        return { type: Process_Overtime_Buly_Result, data : {draw:false,ppTeam1:game.awayPlayer,ppTeam2:game.homePlayer}}
    }

    console.log('Overtime buly results',gamePlay.overtimeBulyThrowResults);
    console.log(getSortedKeys(gamePlay.overtimeBulyThrowResults));
}

function endGame(phase) {
    return {type : End_Game, data : {phase : GamePhases.indexOf(End_Game)}};
}

function getNextPeriod(phase,game) {
    console.log('phase',phase);
    return parseInt(GamePhases[phase].charAt(1));
}

function getNextActiveTeam(phase,game) {
    switch(GamePhases[phase].charAt(2))
    {
        case 'H' : return game.homePlayer;
        case 'A' : return game.awayPlayer;
        case 'W' : return game.ppTeam1;
        case 'L' : return game.ppTeam2;
        default : return game.activeTeam;
    }
}

/**
 'HD1A','HD2S','HD2A','HRWS','HRWA','HCS','HCA','HLWS','HLWA','HD1S',
 'AD1A','AD2S','AD2A','ARWS','ARWA','ACS','ACA','ALWS','ALWA','AD1S'
 */

function getNextActiveAssister(phase,game) {
    switch(GamePhases[phase].substr(2,4)) {
        case 'HD1A' : return 1;
        case 'HD2A' : return 0;
        case 'HLWA' : return 2;
        case 'HCTA' : return 4;
        case 'HRWA' : return 3;
        case 'AD1A' : return 1;
        case 'AD2A' : return 0;
        case 'ALWA' : return 2;
        case 'ACTA' : return 4;
        case 'ARWA' : return 3;
        case 'WLWA' : return 2;
        case 'LD1A' : return 1;
        case 'LLWA' : return 2;
        case 'WD1A' : return 1;
        default : return game.activeAssister;
    }
}

function getNextActiveShooter(phase,game) {
    switch(GamePhases[phase].substr(2,4)) {
        case 'HD1A' : return 2;
        case 'HD2A' : return 1;
        case 'HRWA' : return 0;
        case 'HCTA' : return 3;
        case 'HLWA' : return 4;
        case 'AD1A' : return 2;
        case 'AD2A' : return 1;
        case 'ARWA' : return 0;
        case 'ACTA' : return 3;
        case 'ALWA' : return 4;
        case 'WLWA' : return 4;
        case 'LD1A' : return 0;
        case 'LLWA' : return 4;
        case 'WD1A' : return 0;
        default : return game.activeShooter;
    }
}

function getNextActiveGoalTender(phase, game) {
    if(GamePhases[phase].substr(0,1)!='F'){
        return 5;
    }
    return null;
}

function getNextAction(phase,game) {
    let phaseName = GamePhases[phase];

    switch(phaseName.charAt(phaseName.length - 1)) {
        case 'A' : return 'assistance';
        case 'S' : return 'shoot';
        case 'B' : return 'buly';
        case 'F' : return 'fight';
        case 'P' : return 'punch';
    }
}

function getNextFightPeriod(phase,game) {
    let phaseName = GamePhases[phase];

    switch(phaseName){
        case 'F1H1FF' :
        case 'F1A1FF' :
        case 'F1WFP' : return 1;
        case 'F2H2FF' :
        case 'F2A2FF' :
        case 'F2WFP' : return 2;
    }

    return 0;
}

function getSortedKeys(obj) {
    var keys = []; for(var key in obj) keys.push(key);
    return keys.sort(function(a,b){return obj[b]-obj[a]});
}
