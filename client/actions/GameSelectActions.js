export const Start_Fetching_GamesList = 'Start_Fetching_GamesList';
export const Fetch_GamesList_Success = 'Fetch_GamesList_Success';
export const Fetch_GamesList_Fail = 'Fetch_GamesList_Fail';
export const Reload_GamesList_Page = 'Reload_GamesList_Page';
export const Game_Selected = 'Game_Selected';

export function startFetchingGamesList() {
    return { type: Start_Fetching_GamesList };
}

export function fetchGamesList() {
    return {type:'server/gamesList'};

    return (dispatch) => {
        dispatch(startFetchingGamesList());
    };
}

export function gameSelected(game,team) {
    return {type : 'server/gameSelected', id:game, team:team};
    //return {type : Game_Selected, game};
}

export function fetchGamesListSuccess(gamesList) {
    return { type: Fetch_GamesList_Success, gamesList };
}

export function fetchGamesListFail(error) {
    return { type: Fetch_GamesList_Fail, error };
}

export function reloadingGamesListPage() {
    return { type: Reload_GamesList_Page };
}