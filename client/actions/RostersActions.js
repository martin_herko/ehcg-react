export const Init_Rosters = 'Init_Rosters';
export const Team_Player_List_Loaded = 'Team_Player_List_Loaded';
export const Show_Available_Positions_For_Player= 'Show_Available_Positions_For_Player';
export const AssignPlayerPositionClick = 'AssignPlayerPositionClick';
export const AssignPlayerPositionError = 'AssignPlayerPositionError';
export const AssignGoaltenderByDiceDraw = 'AssignGoaltenderByDiceDraw';
export const GameRostersUpdate = 'GameRostersUpdate';
export const GameRostersUpdateError = 'GameRostersUpdate';
export const ChangeTeamRosterPeriod = 'ChangeTeamRosterPeriod';
export const TeamRosterCompleted = 'TeamRosterCompleted';

export function initRosters(game) {
	return { type: Init_Rosters, data:game}
}

export function fetchTeamAvailPlayers(teamId, gameId) {
	return {type:'server/teamRostersAvailPlayerList', teamId:teamId, gameId:gameId};
}

export function teamPlayerListLoaded(teamList, teamId) {
	return { type:Team_Player_List_Loaded, teamList : teamList, teamId:teamId};
}

export function showAvailablePositionsForPlayer(playerData, role) {
	return { type:Show_Available_Positions_For_Player, playerData:playerData, role:role};
}

export function assignPlayerPositionClick(rostersData, playerData, position) {
	var data = getDataForAssignPlayerPosition(rostersData, playerData, position);
	console.log('assignPlayerPositionClick', data);
	if(data) {
		return {type: 'server/assignPlayerPosition', data:data};
	} else {
		return {type: AssignPlayerPositionError};
	}
}

export function processAssignPlayerPositionClickResult(gameData,result) {
	console.log('processAssignPlayerPositionClickResult',result);
	if (result){
		return fetchGameRosters(gameData.game_id);
	} else {
		return {type: GameRostersUpdateError};
	}
}

export function processGameRostersGoaltenderDiceDrawResult(data) {
	return {type : AssignGoaltenderByDiceDraw, data:data};
}

export function fetchGameRosters(gameId) {
	return {type: 'server/FetchGameRosters', gameId:gameId}
}

export function gameRostersUpdate(data) {
	var rosters = {};
	for(var key in data){
		let teamId = data[key].team_id;
		let period = data[key].period;
		let position = data[key].position;

		if(typeof(rosters[teamId])=== "undefined"){
			rosters[teamId] = {1:{},2:{},3:{},4:{},5:{}};
		}

		rosters[teamId][period][position] = data[key];
	}

	console.log('gameRostersUpdate',rosters);

	return {type : GameRostersUpdate, data:rosters};
}

export function changeTeamRosterPeriod(period, role) {
	return {type : ChangeTeamRosterPeriod, period:period, role:role};
}

export function removePlayerFromRosterPositionClick(playerData) {
	return {type: 'server/removePlayerFromRosterPosition', data:playerData};
}

export function teamRosterCompletedClick(role,roster,gameData) {
	var isvalid = checkIfRosterIsValid(roster);
	var teamId = role=='home' ? gameData.home_team : gameData.away_team;
	//return {type: TeamRosterCompleted, role: role, result:isvalid};
	return {type: 'server/teamRosterCompleted', gameId:gameData.id, teamId:teamId, role:role};
}

function checkIfRosterIsValid(roster) {
	console.log('checkIfRosterIsValid',roster);
	if(typeof(roster) === 'undefined'){
		return false;
	}

	var linesToCheck = [1,2];
	var positionsToCheck = ['d1','d2','lw','c','rw','g'];

	for(var l in linesToCheck){
		if(typeof(roster[linesToCheck[l]]) ==='undefined'){
			return false;
		}
		for(var p in positionsToCheck){
			if(typeof(roster[linesToCheck[l]][positionsToCheck[p]]) ==='undefined'){
				return false;
			}
		}
	}

	return true;
}

export function requestGoaltenderDiceDraw(gameId, teamId) {
	return {type : 'server/requestGoaltenderDiceDraw', gameId:gameId, teamId:teamId};
}

function getDataForAssignPlayerPosition(rostersData,playerData,position) {
	if(validateTargetPosition(playerData,position)){
		var period = rostersData.gameData.home_team == playerData.team_id ? rostersData.homeTeamPeriod : rostersData.awayTeamPeriod;

		return {game_id : rostersData.gameData.id, team_id:playerData.team_id,
			period:period, player_id:playerData.id, position:position};
	} else return false;
}

function validateTargetPosition(playerData,target_position) {
	var player_positions = playerData.position.split(',');
	for(var key in player_positions) {
		var accaptable_positions = getAccaptablePositionsForPosition(player_positions[key]);
		console.log('accaptable_positions',accaptable_positions);
		if(accaptable_positions.indexOf(target_position)>=0){
			return true;
		}
	}

	return false;
}

function getAccaptablePositionsForPosition(position) {
	switch(position){
		case 'd' : return ['d1','d2'];
		case 'lw' :
		case 'c' :
		case 'rw' : return ['lw','c','rw'];
		case 'g' : return ['g'];
	}

	return [];
}