import { GamePhases } from './Constants/GamePhases';

export const Throw_dice_result = 'Throw_dice_result';
export const Update_Scoreboard = 'Update_Scoreboard';
export const Update_Game = 'Update_game';
export const Action_Shoot = 'shoot';
export const Action_Assistance = 'assistance';
export const Action_Fight = 'fight';
export const Overtime_Periods = [3,4,5];

export function throwDice(gameData) {
    console.log('gameData',gameData);
    console.log('action',gameData.action);
    var diceResult = getDiceResult(gameData.action);

    if (gameData.activeTeam == gameData.homePlayer) {
        var teamRoster = gameData.homePlayerRoster;
        var opponentTeamRoster = gameData.awayPlayerRoster;
    }
    if (gameData.activeTeam == gameData.awayPlayer) {
        var teamRoster = gameData.awayPlayerRoster;
        var opponentTeamRoster = gameData.homePlayerRoster;
    }

    console.log('teamRoster',teamRoster);

    var playersAttrValues = getPlayersAttributeValues(gameData,teamRoster,opponentTeamRoster);
    var resultAbsolute = getAbsoluteResult(gameData, diceResult, playersAttrValues);
    var result = getResult(resultAbsolute);
    var bonuses = setBonuses(gameData, result, diceResult);

    console.log('result',result);
    //Update score
    var homeTeamScore = gameData.homeScore;
    var awayTeamScore = gameData.awayScore;

    if(gameData.action == Action_Shoot) {
        homeTeamScore = (gameData.activeTeam == gameData.homePlayer) && result ? (gameData.homeScore + 1) : gameData.homeScore;
        awayTeamScore = (gameData.activeTeam == gameData.awayPlayer) && result ? (gameData.awayScore + 1) : gameData.awayScore;
    }

    var ppFirstTeamScored = 'undefined';
    if(GamePhases[gameData.phase] == '03WCTS' || GamePhases[gameData.phase] == '04WCTS' || GamePhases[gameData.phase] == '05WCTS'){
        ppFirstTeamScored = result;
    }

    let activeAssister = gameData.activeAssister!=null ? teamRoster[gameData.period][gameData.activeAssister].name : null;
    let activeShooter = gameData.activeShooter!=null ? teamRoster[gameData.period][gameData.activeShooter].name : null;
    let activeGoalTender = gameData.activeGoalTender!=null ? teamRoster[gameData.period][gameData.activeGoalTender].name : null;

    console.log('result detail', diceResult);

    return { type: Throw_dice_result, data:{
        action:gameData.action,
        phase:gameData.phase,
        diceResult:diceResult,
        activeTeam:gameData.activeTeam,
        activeAssister : activeAssister,
        activeShooter : activeShooter,
        activeGoalTender : activeGoalTender,
        attacker_attr_value : playersAttrValues.attacker_attr_value,
        attacker_bonuses_value : playersAttrValues.attacker_bonuses_value,
        goaltender_attr_value:playersAttrValues.goaltender_attr_value,
        goaltender_bonuses_value:playersAttrValues.goaltender_bonuses_value,
        resultAbsolute : resultAbsolute,
        bonuses : bonuses,
        result:result,
        homeTeamScore:homeTeamScore,
        awayTeamScore:awayTeamScore,
        period:gameData.period,
        ppFirstTeamScored:ppFirstTeamScored
    }};
}

export function updateScoreBoard(homeScore, awayScore) {
    return { type: Update_Scoreboard, data : {homeScore:homeScore, awayScore:awayScore} }
}

export function updateGame(results) {
    console.log('updateGame-results',results);
    return { type: Update_Game, data : results}
}

export function logStats(result) {

}

function getDiceResult(action) {
    let diceResult = null;
    switch(action){
        case 'fight' : {
            let firstDiceResult =  Math.floor( Math.random() * ( 1 + 20 - 1 ) ) + 1;
            let secondDiceResult = Math.floor( Math.random() * ( 1 + 20 - 1 ) ) + 1;
            diceResult = {firstDice:firstDiceResult, secondDice:secondDiceResult, sum:firstDiceResult+secondDiceResult};
        }
            break;
        default : diceResult = {sum: Math.floor( Math.random() * ( 1 + 20 - 1 ) ) + 1};
    }

    console.log('diceResult',diceResult);
    return diceResult;
}

function getPlayersAttributeValues(gameData, teamRoster, opponentTeamRoster) {
    let attacker_attr_value = 0;
    let goaltender_attr_value = 0;

    switch(gameData.action) {
        case Action_Assistance : {
            attacker_attr_value = teamRoster[gameData.period][gameData.activeAssister].atr2;
        } break;
        case Action_Shoot : {
            switch(gameData.period){
                case 1 :
                case 2 :
                {
                    attacker_attr_value = teamRoster[gameData.period][gameData.activeShooter].atr1;
                }break;
                //Overtime
                case 3 :
                case 4 :
                case 5 :
                {
                    attacker_attr_value = -3;
                }
            }
            goaltender_attr_value = getGoalTenderAttrValue(gameData.period,opponentTeamRoster);
        } break;
        case Action_Fight : {
            if(gameData.activeTeam == gameData.homeFighter.team_id){
                attacker_attr_value = gameData.homeFighter.fight;
            } else {
                attacker_attr_value = gameData.awayFighter.fight;
            }
        } break;
    }

    let bonuses = applyPhaseBonuses(gameData);
    let attacker_bonuses_value = bonuses.shoot;
    let goaltender_bonuses_value = bonuses.defense;

    console.log('attacker_attr_value', attacker_attr_value);
    console.log('goaltender_attr_value', goaltender_attr_value);

    return {
        attacker_attr_value : attacker_attr_value,
        attacker_bonuses_value : attacker_bonuses_value,
        goaltender_attr_value : goaltender_attr_value,
        goaltender_bonuses_value : goaltender_bonuses_value
    }
}

function applyPhaseBonuses(gameData) {
    let shoot = 0;
    let assistance = 0;
    let defense = 0;

    if(gameData.bonuses[gameData.activeTeam][gameData.phase] !='undefined'){
        let bonuses = gameData.bonuses[gameData.activeTeam][gameData.phase];
        if(bonuses.shoot.sum != 'undefined'){
            shoot += bonuses.shoot.sum;
        }
        if(bonuses.defense.sum != 'undefined'){
            defense += bonuses.defense.sum;
        }
    }

    return {shoot:shoot,assistance:assistance,defense:defense};
}

function getGoalTenderAttrValue(period,roster) {
    console.log('goaltender',roster[period][5]);
    switch(period){
        case 1 : return roster[period][5].atr1;
        case 2 : return roster[period][5].atr2;
        case 3 : return roster[period][5].atr3;
    }
}

function getAbsoluteResult(gameData, diceResult, playersAttrValues) {
    let attacker_attr_value = playersAttrValues.attacker_attr_value;
    let attacker_bonuses_value = playersAttrValues.attacker_bonuses_value;
    let goaltender_attr_value = playersAttrValues.goaltender_attr_value;
    let goaltender_bonuses_value = playersAttrValues.goaltender_bonuses_value;

    attacker_attr_value = applyOvertimeAttackerShooterRules(gameData.period,gameData.action,diceResult.sum,attacker_attr_value);

    switch(gameData.action)
    {
        case Action_Assistance : return diceResult.sum + attacker_attr_value + attacker_bonuses_value + goaltender_attr_value + goaltender_bonuses_value;
        case Action_Shoot : return diceResult.sum + attacker_attr_value + attacker_bonuses_value + goaltender_attr_value + goaltender_bonuses_value;
        case Action_Fight : return diceResult.sum + attacker_attr_value + attacker_bonuses_value;
    }
}

function getResult(resultAbsolute) {
    return resultAbsolute>=15 ? true:false;
}

function applyOvertimeAttackerShooterRules(period,action,diceResult,attacker_attr_value)
{
    //Ak v presilovke pri nahravke padne na kocke 20-tka, tak strela je -1, nie -3
    if((Overtime_Periods.indexOf[period] != -1) && (action == Action_Shoot) && diceResult==20){
        return -1;
    }
    else return attacker_attr_value;
}

function getAssistanceBonus(result, diceResult) {
    if(result) {
        switch(diceResult) {
            case 20 : return 2;
            case 1 : return -1;
            default : return 1;
        }
    } else {
        return 0;
    }
}

export function getInitBonuses(homeTeam,awayTeam) {
    let bonuses = {};
    let teams = [homeTeam,awayTeam];
    let actions = ['shoot','assistance','defense'];

    // f.e. team 24 / 1st period / shoot / attack / 0
    for(var t=0;t<teams.length;t++) {
        bonuses[teams[t]] = {};
        for (var p = 0, len = GamePhases.length; p < len; p++) {
            bonuses[teams[t]][p] = {
                shoot:getDefaultBonus(),
                assistance:getDefaultBonus(),
                defense:getDefaultBonus()
            };
        }
    }
    return bonuses;
}

function getDefaultBonus() {
    return {values:{},sum:0}
}

function setBonuses(gameData, result, diceResult){
    let assistanceBonus = getAssistanceBonus(result, diceResult.sum);
    let bonuses = gameData.bonuses;
    bonuses[gameData.activeTeam][gameData.phase+1].shoot.values['assistance']=assistanceBonus;
    bonuses[gameData.activeTeam][gameData.phase+1].shoot.sum = object_sum(bonuses[gameData.activeTeam][gameData.phase+1].shoot.values);

    return bonuses;
}

function object_sum(obj){
    var sum = 0;
    for( var el in obj ) {
        if( obj.hasOwnProperty( el ) ) {
            sum += parseFloat( obj[el] );
        }
    }
    return sum;
}

function array_sum(array){
    var total = 0;
    for (var i = 0, n = array.length; i < n; ++i)
    {
        total += array[i];
    }

    return total;
}
