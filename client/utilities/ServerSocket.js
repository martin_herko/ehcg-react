import { receivedAllUniversalTodos, optimisticUniversalAddSuccess } from '../actions/TodoActions';
import { fetchTeamsListSuccess } from '../actions/TeamSelectActions';
import { fetchGamesListSuccess } from '../actions/GameSelectActions';
import { joinToGameSuccess, gameUpdate, teamRosterLoaded} from '../actions/GameActions';
import { teamPlayerListLoaded, processAssignPlayerPositionClickResult, processGameRostersGoaltenderDiceDrawResult, gameRostersUpdate, fetchGameRosters, fetchTeamAvailPlayers } from '../actions/RostersActions';

/*import io from 'socket.io-client';
var socket = io();*/

export function linkSocketToStore(dispatch,socket) {
	// Add listeners that dispatch actions here.
	socket.on("current-universal-todos", function(result){
		// console.log("got all todos!", result);
		if(result.error){
			//TODO handle some how
			alert("something went wrong retrieving all todos");
		} else {
			dispatch(receivedAllUniversalTodos(result.todos));
		}
	});

	socket.on("new-todo", function(result){
		// console.log("got a new todo!", result);
		if(result.error){
			//Do nothing
		} else {
			dispatch(optimisticUniversalAddSuccess(result.todo));
		}
	});

	socket.on('action', function(result){
		console.log(result);
		if(result.error){
			//Do nothing
		} else {
			switch(result.type)
			{
				case 'teamsList' : dispatch(fetchTeamsListSuccess(result.data));
					break;
				case 'gamesList' : dispatch(fetchGamesListSuccess(result.data));
					break;
				case 'joinedToGame' : dispatch(joinToGameSuccess(result.data));
					break;
				case 'gameUpdate' : dispatch(gameUpdate(result.data));
					break;
				case 'teamRoster' : dispatch(teamRosterLoaded(result.data, result.role));
					break;
				case 'teamPlayerList' : dispatch(teamPlayerListLoaded(result.data, result.teamId));
					break;
				case 'assignPlayerPositionResult' : {
					dispatch(processAssignPlayerPositionClickResult(result.data, result.result));
					if(result.result){
						dispatch(fetchTeamAvailPlayers(result.data.team_id, result.data.game_id));
					}
				}
					break;
				case 'removePlayerFromRosterPosition' : {
					if(result.result){
						dispatch(fetchTeamAvailPlayers(result.data.team_id, result.data.game_id));
						dispatch(fetchGameRosters(result.data.game_id));
					}
				}
					break;
				case 'gameRostersGoaltenderDiceDrawResult' : {
					if(result.result){
						dispatch(processGameRostersGoaltenderDiceDrawResult(result.data));
						dispatch(fetchTeamAvailPlayers(result.data.team_id, result.data.game_id));
						dispatch(fetchGameRosters(result.data.game_id));
					}
				}	break;
				case 'gameRostersUpdate' : dispatch(gameRostersUpdate(result.data.rosters));
					break;
				case 'message' : console.log('message',result.data);
					break;
			}

		}
	});

}
