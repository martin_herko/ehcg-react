import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class UserDropdown extends Component {
  render() {
    return (
      <li className="dropdown">
        <a href="#" className="dropdown-toggle" 
                    data-toggle="dropdown" 
                    role="button" 
                    aria-haspopup="true" 
                    aria-expanded="false">

                    Moje konto <span className="caret"></span>
        </a>

        <ul className="dropdown-menu">
          <li><Link to="/settings"> Nastavenia </Link></li>
          <li role="separator" className="divider"></li>
          <li onClick={this.props.logout}><a href="#">Odhl�si�</a></li>
        </ul>
      </li>
    );
  }
}

export default class Navbar extends Component {
  render() {
    var gameMenu;
    var gameRosters;
    var todos;
    var loginTab;
    var signupTab;
    var userDropdown;
    if (this.props.userAuthSession.isLoggedIn) {
      gameMenu = <li><Link to="/game-menu"> Hrať </Link></li>
      todos =  <li><Link to="/dash"> Úlohy </Link></li>;
      userDropdown = (<UserDropdown logout={this.props.logout}/>);
    } else {
      loginTab = <li><Link to="/login"> Prihlásiť </Link></li>;
      signupTab = <li><Link to="/signup"> Registrovať</Link></li>;
    }

    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">

          <div className="navbar-header">
            <Link className="navbar-brand" to="/">EHCG</Link>
          </div>

          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav navbar-right"> 

              { gameMenu }
              { gameRosters }
              { todos }

              <li><Link to="/about">O hre</Link></li>

              { loginTab }
              { signupTab }

              { userDropdown }
              
            </ul>
          </div>

        </div>
      </nav>

    );
  }
}
