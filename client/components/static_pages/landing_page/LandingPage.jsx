import './LandingPage.css';
import React, { Component, PropTypes } from 'react';

class LandingPageHeader extends Component {
    render() {
        return (
            <div className="landing-page-header-container">
                <div className="landing-page-header-container-title col-xs-12">
                    EHCG
                </div>
                <div className="landing-page-header-container-subtitle col-xs-12">
                    European Hockey Card Game
                </div>
            </div>);
    }
}

class LandingPageColumn extends Component {
    render() {
        return (
            <div className="col-lg-4">
                <img className="img-responsive" src={this.props.imageSrc} alt="Generic placeholder image"/>

                <h2>{this.props.heading}</h2>

                <p>{this.props.text}</p>
            </div>
        );
    }
}

class LandingPageThreeColumnSection extends Component {
    render() {
        return (
            <div className="container landing-page-three-column-section">
                {/*<div className="row">
                    <LandingPageColumn
                        heading={""}
                        text={""}
                        imageSrc={"/images/titleLogo.jpg"}
                        />
                </div>*/}
            </div>
        );
    }
}


export default class LandingPage extends Component {
    render() {
        return (
            <div className="">
                <LandingPageHeader />

                <LandingPageThreeColumnSection />

            </div>
        );
    }
}
