import './GameRosterListbox.css';

import React, { Component} from 'react';

export default class RosterPlayersListbox extends Component {
    constructor(props) {
        super(props);
    }

    onPlayerOptionClick(playerData, role) {
        const {hasUserControl} = this.props;
        if(hasUserControl) {
            if (playerData.position != 'g') {
                this.props.onPlayerOptionClick(playerData, role);
            }
        }
    }

    onGoaltenderDiceDraw() {
        this.props.onGoaltenderDiceDraw();
    }

    render() {

        const { role, selectedPlayerData, playerList, assignedGoaltenderData, hasUserControl} = this.props;

        var playerListOptions = [];
        var _this = this;
        var selectedPlayerDataId = null;
        if(selectedPlayerData!==null && typeof(selectedPlayerData) != 'undefined'){
            console.log(selectedPlayerData);
            selectedPlayerDataId = selectedPlayerData.id;
        }

        if(hasUserControl){
            var goaltenderDiceResultDiv = <button onClick={()=>this.onGoaltenderDiceDraw()}>Losovať brankára</button>;
        }else{
            var goaltenderDiceResultDiv = <span></span>;
        }

        console.log(assignedGoaltenderData);
        if(assignedGoaltenderData!=false){
            var goaltenderDiceResultDiv = <span>Hod kockou : {assignedGoaltenderData.dice_result}</span>;
        }

        var rosterPlayerItemClass = hasUserControl ? "rosterPlayerListBoxItem" : "rosterPlayerDisabledListBoxItem";

        if (playerList.length > 0) {
            var playerListOptions =
                <div id={role + "playersListbox"}>
                    <span>Brankári</span>
                    <div className="rosterGoaltenderList">
                        {
                            playerList.map(function (playerData) {
                                if(playerData.position==='g') {
                                    console.log('rosterGoaltenderList',playerData);
                                    return <div className="rosterGoaltenderListItem" key={playerData.id}>
                                        <span>{playerData.position} | {playerData.name} | {playerData.clk} | {playerData.team_position_priority} | {playerData.atr1} | {playerData.atr2} | {playerData.atr3}</span>
                                    </div>
                                }
                            })
                        }
                    </div>
                    {goaltenderDiceResultDiv}
                    <hr/>
                    <span>Hráči</span>
                    <div className="rosterPlayerListbox" size="18">
                        {
                            playerList.map(function (playerData) {
                                if(playerData.position!='g') {
                                    return <div
                                        className={rosterPlayerItemClass + (selectedPlayerDataId == playerData.id ? "Selected" : "")}
                                        key={playerData.id}
                                        onClick={()=>_this.onPlayerOptionClick(playerData,role,playerData.id)}>
                                        {playerData.position} | {playerData.name} | {playerData.clk} | {playerData.atr1} {playerData.atr2} {playerData.atr3}
                                    </div>
                                };
                            })
                        }
                    </div>
                </div>
        }

        return (
            <div id={role + "playersListbox"}>
                {playerListOptions}
            </div>
        );
    }
}