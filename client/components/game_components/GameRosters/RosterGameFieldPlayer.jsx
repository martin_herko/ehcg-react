import React, { Component} from 'react';

export class RosterGameFieldPlayer extends Component {
    constructor(props) {
        super(props);
    }

    onAssignPlayerPositionClick(selectedPlayerData, rosterPosition) {
        this.props.onAssignPlayerPositionClick(selectedPlayerData,rosterPosition);
    }

    onRemovePlayerFromPositionClick(playerData) {
        if(playerData.position!='g') {
            this.props.onRemovePlayerFromPositionClick(playerData);
        }
    }

    render() {
        const{ role, active, playerData, rosterPosition, selectedPlayerData } = this.props;
        var rosterPositionName = getPlayerPositionNameByRosterPosition(rosterPosition);

        return (
            <div className="rosterPlayer" id={"roster" + role + rosterPositionName}>
                <div className={"rosterPlayerPosition col-xs-4" + (active!='' ? " rosterPlayer" + active : '')}
                     onClick={()=>this.onAssignPlayerPositionClick(selectedPlayerData,rosterPosition)}>
                    <span>{playerData.position}</span>
                    <span className="rosterPlayerPositionDelete"></span>
                </div>
                <div className="rosterPlayerAttributes col-xs-8">
                    <div className="row">
                        <div className="col-xs-4 no-padding">{playerData.atr1}</div>
                        <div className="col-xs-4 no-padding">{playerData.atr2}</div>
                        <div className="col-xs-4 no-padding">{playerData.atr3}</div>
                        <div className="col-xs-6 no-padding">{playerData.fight>0 ? 'F'+playerData.fight : ''}</div>
                        <div className="col-xs-6 no-padding">{playerData.enf>0 ? 'ENF' : ''}</div>
                    </div>
                </div>
                <div className="rosterPlayerName col-xs-12" onClick={()=>this.onRemovePlayerFromPositionClick(playerData)}>{playerData.name}</div>
            </div>
        )
    }

}

function getPlayerPositionNameByRosterPosition(rosterPosition){
    switch(rosterPosition){
        case 'g' : return 'Goaltender';
        case 'd1': return 'Defenseman1';
        case 'd2': return 'Defenseman2';
        case 'rw': return 'RightWing';
        case 'lw': return 'LeftWing';
        case 'c': return 'Center';
    }
}