import './GameRosterField.css';
import './GameRosterPlayers.css';

import React, { Component} from 'react';
import {RosterGameFieldPlayer} from './RosterGameFieldPlayer';

export default class PlayerRosterGameField extends Component {
    constructor(props) {
        super(props);
    }

    onAssignPlayerPositionClick(playerData,position) {
        const {hasUserControl} = this.props;
        if(hasUserControl) {
            this.props.onAssignPlayerPositionClick(playerData, position);
        }
    }

    onRemovePlayerFromPositionClick(playerData) {
        const {hasUserControl} = this.props;
        if(hasUserControl) {
            this.props.onRemovePlayerFromPositionClick(playerData);
        }
    }

    render() {

        const { role, showAvailablePositions, selectedPlayerData, gamePeriodRosters, hasUserControl} = this.props;

        var GoaltenderActive = showAvailablePositions ? 'Disabled' : '';
        var DefenseManActive = showAvailablePositions ? 'Disabled' : '';
        var LeftWingActive = showAvailablePositions ? 'Disabled' : '';
        var RightWingActive = showAvailablePositions ? 'Disabled' : '';
        var CenterActive = showAvailablePositions ? 'Disabled' : '';

        if(showAvailablePositions && hasUserControl){
            var playerDataPositions = selectedPlayerData.position.split(',');
            for(var key in playerDataPositions) {
                console.log(playerDataPositions[key]);
                switch(playerDataPositions[key]){
                    case 'g' : GoaltenderActive = 'Full';
                        break;
                    case 'd' : DefenseManActive = 'Full';
                        break;
                    case 'lw' : LeftWingActive = 'Full';
                        RightWingActive = RightWingActive=='Full' ? 'Full':'Limited';
                        CenterActive = CenterActive=='Full' ? 'Full':'Limited';
                        break;
                    case 'rw' : RightWingActive = 'Full';
                        LeftWingActive = LeftWingActive=='Full' ? 'Full':'Limited';
                        CenterActive = CenterActive=='Full' ? 'Full':'Limited';
                        break;
                    case 'c' : CenterActive = 'Full';
                        LeftWingActive = LeftWingActive=='Full' ? 'Full':'Limited';
                        RightWingActive = RightWingActive=='Full' ? 'Full':'Limited';
                        break;
                }
            }
        }

        var players = {d1:{name:'',attr:'',position:'d1'},d2:{name:'',attr:'',position:'d2'},lw:{name:'',attr:'',position:'lw'},
            c:{name:'',attr:'',position:'c'},rw:{name:'',attr:'',position:'rw'},g:{name:'',attr:'',position:'g'}};

        for(var key in players){
            if(gamePeriodRosters!=null && typeof(gamePeriodRosters[key])!='undefined'){
                players[key] = gamePeriodRosters[key];
            }
        }

        console.log('players',players);

        return (
            <div id={role + "PlayerRoster"}>
                <div className="row" id="gameRoster">
                    <div className="row" id="gameRosterField">
                        <div className="row" id="players">
                            <RosterGameFieldPlayer role={role} active={GoaltenderActive} playerData={players['g']} rosterPosition={'g'} selectedPlayerData={selectedPlayerData}
                                                   onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                                   onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                />
                            <RosterGameFieldPlayer role={role} active={DefenseManActive} playerData={players['d1']} rosterPosition={'d1'} selectedPlayerData={selectedPlayerData}
                                                   onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                                   onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                />
                            <RosterGameFieldPlayer role={role} active={DefenseManActive} playerData={players['d2']} rosterPosition={'d2'} selectedPlayerData={selectedPlayerData}
                                                   onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                                   onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                />
                            <RosterGameFieldPlayer role={role} active={LeftWingActive} playerData={players['lw']} rosterPosition={'lw'} selectedPlayerData={selectedPlayerData}
                                                   onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                                   onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                />
                            <RosterGameFieldPlayer role={role} active={RightWingActive} playerData={players['rw']} rosterPosition={'rw'} selectedPlayerData={selectedPlayerData}
                                                   onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                                   onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                />
                            <RosterGameFieldPlayer role={role} active={CenterActive} playerData={players['c']} rosterPosition={'c'} selectedPlayerData={selectedPlayerData}
                                                   onAssignPlayerPositionClick={(playerData,position)=>this.onAssignPlayerPositionClick(playerData,position)}
                                                   onRemovePlayerFromPositionClick={(playerData)=>this.onRemovePlayerFromPositionClick(playerData)}
                                />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}