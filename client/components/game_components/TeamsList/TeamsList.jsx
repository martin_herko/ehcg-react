import React, { Component, PropTypes } from 'react';

import { fetchTeamsList, reloadingTeamsListPage } from '../../../actions/TeamSelectActions';
import './TeamsList.css';

export default class TeamsList extends Component {
    constructor(props) {
        super(props);
        console.log('teamslist props constructor', this.props);
        this.teamSelected = this.teamSelected.bind(this);
    }

    teamSelected(team) {
        this.props.onTeamSelected(team);
    }

    render() {
        var TeamList;
        var teamObjects = this.props.teamsListdata;
        console.log('teamslist props', this.props);
        console.log('teamObjects', teamObjects);
        var _this = this;

        if (Object.keys(teamObjects).length === 0) {
            TeamList = <p> Neboli nájdené žiadne aktívne tímy. </p>;
        }
        else if (Object.keys(teamObjects).length == 1 && (typeof teamObjects[0] !== 'undefined')) {
            console.log(teamObjects);
            var team = teamObjects[0];
            TeamList = <div>
                <div className="col-xs-3">{team.name}</div>
                <div className="col-xs-3"><img src={'/images/teams/'+team.code+'.gif'} /></div>
            </div>;
        }
        else {
            TeamList = [];

            Object.keys(teamObjects).forEach(function (team, value) {
                if (typeof teamObjects[team].code !== 'undefined') {
                    TeamList.push(
                        <div className="col-xs-3" key={teamObjects[team].code} title={teamObjects[team].name}>
                            <img onClick={() => _this.teamSelected(teamObjects[team])} src={'/images/teams/'+teamObjects[team].code+'.gif'}/>
                        </div>);
                }
            });
            TeamList = <div className="row col-xs-12"> {TeamList} </div>;
        }

        console.log(TeamList);

        return (
            <div id="teamList">
                <p>Vyberte tim</p>
                {TeamList}
            </div>
        );
    }
}
