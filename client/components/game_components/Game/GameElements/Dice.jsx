import React, { Component} from 'react';
/*import {Rectangle, Circle, Ellipse, Line, Polyline, CornerBox, Triangle} from 'react-shapes';*/


export default class Dice extends Component {
    constructor(props) {
        super(props);
    }

    throwDice() {
        this.props.onInitDiceThrow();
    }

    render() {
        //let throwedNumber = this.state.throwedNumber;
        //<Polyline points='0 50,50 100,100 50,50 0,0 50' fill={{color:'#2409ba'}} stroke={{color:'#E65243'}} strokeWidth={3} />
        const { gamePlay } = this.props;

        var dice = gamePlay.diceThrowEnabled ? <button onClick={()=>this.throwDice()}><img height="70" src="/images/icons/dice20.png" className="img responsive" /></button> : '';

        return(
            <div id="dice">
                {dice}
            </div>
        )
    }
}