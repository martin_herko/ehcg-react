import React, { Component} from 'react';

export class PlayerCard extends Component {
    renderAbilities(gameData, playerData) {
        let abilities = <div></div>;
        if(gameData.action=='fight' && playerData.fight>0){
            if (isFighterActive(gameData, playerData)) {
                abilities = <img onClick={()=>this.onPlayerCardClick('fight')} src="/images/icons/fight.png" height="35"
                                 title={'Bitkár ' + playerData.fight + ', kliknutím aktivujete bitku'}></img>
            }
        }

        return abilities;
    }

    onPlayerCardClick(action){
        this.props.onPlayerCardClick(action);
    }

    render() {
        const { gameData , playerData, img } = this.props;

        var cardClass = 'non_active';
        var role = false;

        /*console.log('activeTeam',gameData.activeTeam);
        console.log('player team',playerData.team_id);
        console.log('activeShooter',gameData.activeShooter);
        console.log('player index',playerData.index);*/

        if (gameData.activeTeam == playerData.team_id) {
            if (gameData.activeShooter == playerData.index) {
                cardClass = 'activeShooter';
                role = 'shooter';
            }
            if (gameData.activeAssister == playerData.index) {
                cardClass = 'activeAssister';
                role = 'assister';
            }
            if(gameData.beatingMode && gameData.activeFighter == playerData.index) {
                cardClass = 'activeFighter';
                role = 'fighter';
            }
        } else {
            if(gameData.activeTeam>0 && playerData.team_id>0) {
                if (gameData.activeGoalTender == playerData.index) {
                    cardClass = 'activeGoalTender';
                    role = 'goaltender';
                }
            }

            if(gameData.beatingMode) {
                cardClass = 'playerToBeat';
                role = 'playerToBeat';
            }
        }

        if(typeof gameData.fighters[gameData.homePlayer] !== 'undefined' && typeof gameData.fighters[gameData.awayPlayer] !== 'undefined') {
            if(!gameData.beatingMode && gameData.fightPeriod) {
                if (typeof gameData.fighters[gameData.homePlayer][gameData.fightPeriod][playerData.id] !== 'undefined') {
                    if (gameData.fighters[gameData.homePlayer][gameData.fightPeriod][playerData.id].activated) {
                        cardClass = 'activatedFighter';
                    }
                }
                if (typeof gameData.fighters[gameData.awayPlayer][gameData.fightPeriod][playerData.id] !== 'undefined') {
                    if (gameData.fighters[gameData.awayPlayer][gameData.fightPeriod][playerData.id].activated) {
                        cardClass = 'activatedFighter';
                    }
                }
            }
        }

        return (
            <div className={"playerCard " + cardClass} onClick={()=> {if(role == 'playerToBeat') {this.onPlayerCardClick('punch')}}}>
                <div id="playerTitle">
                    <div className="col-xs-12"><b>{playerData.name}</b></div>
                </div>
                <div id="playerHeader">
                    <div className="col-xs-6 text-left no-padding"><b>{playerData.index}</b>/{playerData.id}</div>
                    <div className="col-xs-6 text-right no-padding">{playerData.position}</div>
                </div>
                <div id="playerPhoto">
                    <div id="playerAbilities">{this.renderAbilities(gameData, playerData)}</div>
                    <img src={"/images/players/" + img + ".png"} className="img-responsive" />
                </div>
                <div id="playerMainAttributes">
                    <div className={"col-xs-4 no-padding " + (role=='shooter' ? 'active':'') + (role=='goaltender' && gameData.period==1 ? 'active':'')}>{playerData.atr1}</div>
                    <div className={"col-xs-4 no-padding " + (role=='assister' ? 'active':'') + (role=='goaltender' && gameData.period==2 ? 'active':'')}>{playerData.atr2}</div>
                    <div className="col-xs-4 no-padding">{playerData.atr3}</div>
                </div>
                <div id="playerAdditionalAttributes">
                    <div className="col-xs-3 no-padding">{playerData.experience}</div>
                    <div className="col-xs-6 no-padding">({playerData.pot1} {playerData.pot2} {playerData.pot3})</div>
                    <div className="col-xs-3 no-padding">{playerData.value}</div>
                </div>
            </div>
        )
    }
}

function isFighterActive(gameData, playerData) {
    let active = false;
    if(typeof gameData.fighters[playerData.team_id][gameData.period][playerData.id] !== 'undefined'){
        //return negation, if activated is false => is active
        return !gameData.fighters[playerData.team_id][gameData.period][playerData.id].activated;
    }

    return active;
}
