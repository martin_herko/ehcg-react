import './Players.css';
import React, { Component} from 'react';
import { PlayerCard } from './PlayerCard';

export class Player extends Component {
    constructor(id){
        super();
        this.id = id;
    }

    onPlayerCardClick(action) {
        console.log(this.props);
        this.props.onPlayerCardClick(action);
    }

    render() {
        const { gameData, playerData, img } = this.props;
        return (
            <div className="player" id={this.id}>
                <PlayerCard gameData={gameData} playerData={playerData} img={img} onPlayerCardClick={(action)=>this.onPlayerCardClick(action)}/>
            </div>
        )
    }
}

export class HomeGoaltender extends Player {
    constructor(){
        super('homeGoaltender');
    }
}

export class HomeDefenseman1 extends Player {
    constructor(){
        super('homeDefenseman1');
    }
}

export class HomeDefenseman2 extends Player {
    constructor(){
        super('homeDefenseman2');
    }
}

export class HomeLeftWing extends Player {
    constructor(){
        super('homeLeftWing');
    }
}

export class HomeRightWing extends Player {
    constructor(){
        super('homeRightWing');
    }
}

export class HomeCenter extends Player {
    constructor(){
        super('homeCenter');
    }
}

export class AwayGoaltender extends Player {
    constructor(){
        super('awayGoaltender');
    }
}

export class AwayDefenseman1 extends Player {
    constructor(){
        super('awayDefenseman1');
    }
}

export class AwayDefenseman2 extends Player {
    constructor(){
        super('awayDefenseman2');
    }
}

export class AwayLeftWing extends Player {
    constructor(){
        super('awayLeftWing');
    }
}

export class AwayRightWing extends Player {
    constructor(){
        super('awayRightWing');
    }
}

export class AwayCenter extends Player {
    constructor(){
        super('awayCenter');
    }
}