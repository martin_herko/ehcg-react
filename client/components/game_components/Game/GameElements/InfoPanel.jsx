import React, { Component} from 'react';

import localStorage from 'store';

export default class InfoPanel extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const { gameData, gamePlay } = this.props;
        var results = [];
        var header = '';

        var teamsList = localStorage.get('teamsList');

        for (var key in gamePlay.diceThrowResults) {
            let data = gamePlay.diceThrowResults[key];
            let resultDiv = '';

            switch (data.action) {
                case 'assistance' :
                    resultDiv =
                        <div key={key} className={"infoPanel-result_row alert-" + (data.result ? "success" : "danger")}>
                                <div className="col-xs-1">Aktivita</div>
                                <div className="col-xs-9">{data.activeAssister} atr.</div>
                                <div className="col-xs-1">Kocka</div>
                                <div className="col-xs-1">Sum</div>
                                <div className="col-xs-1">{data.action}</div>
                                <div className="col-xs-9">{data.attacker_attr_value}</div>
                                <div className="col-xs-1">
                                    <i className="glyphicon glyphicon-unchecked"></i> {data.diceResult.sum}
                                </div>
                                <div className="col-xs-1"><strong>{data.resultAbsolute}</strong></div>
                        </div>;
                    break;
                case 'shoot' :
                    resultDiv =
                        <div key={key} className={"infoPanel-result_row alert-" + (data.result ? "success" : "danger")}>
                                <div className="col-xs-1">Aktivita</div>
                                <div className="col-xs-3">Asist. {data.activeAssister} atr.</div>
                                <div className="col-xs-3">Útoč. {data.activeShooter} atr.</div>
                                <div className="col-xs-3">Brankár {data.activeGoalTender} atr.</div>
                                <div className="col-xs-1">Kocka</div>
                                <div className="col-xs-1">Sum</div>
                                <div className="col-xs-1">{data.action}</div>
                                <div className="col-xs-3">{data.attacker_bonuses_value}</div>
                                <div className="col-xs-3">{data.attacker_attr_value}</div>
                                <div className="col-xs-3">{data.goaltender_attr_value}</div>
                                <div className="col-xs-1">
                                    <i className="glyphicon glyphicon-unchecked"></i> {data.diceResult.sum}
                                </div>
                                <div className="col-xs-1"><strong>{data.resultAbsolute}</strong></div>
                        </div>;
                    break;
                case 'buly' :
                    resultDiv =
                        <div key={key} className={"infoPanel-result_row alert-info"}>
                            <div className="col-xs-1">Aktivita</div>
                            <div className="col-xs-9 text-left">Tím</div>
                            <div className="col-xs-2 text-left">Kocka</div>
                            <div className="col-xs-1">Buly</div>
                            <div className="col-xs-9 text-left">{teamsList[data.activeTeam].code}</div>
                            <div className="col-xs-2 text-left">
                                <i className="glyphicon glyphicon-unchecked"></i> {data.diceResult.sum}
                            </div>
                        </div>;
                    break;
                case 'fight' :
                    resultDiv =
                        <div key={key} className={"infoPanel-result_row alert-info"}>
                            <div className="col-xs-1">Aktivita</div>
                            <div className="col-xs-7 text-left">Tím</div>
                            <div className="col-xs-2 text-left">Kocka 2x</div>
                            <div className="col-xs-2 text-left">Spolu</div>
                            <div className="col-xs-1">Bitka</div>
                            <div className="col-xs-7 text-left">{teamsList[data.activeTeam].code}</div>
                            <div className="col-xs-2 text-left">
                                <i className="glyphicon glyphicon-unchecked"></i> {data.diceResult.sum} + {data.attacker_attr_value}
                            </div>
                            <div className="col-xs-2 text-left">{data.resultAbsolute}</div>
                        </div>;
                    break;
            }
            results.push(resultDiv);
        }

        results.reverse();

        if(results.length>0){
            header = <div className="col-xs-12 infoPanel-header">Štatistiky</div>;
        }


        return (
            <div className="row" id="infoPanel">
                {header}
                {results}
            </div>
        )
    }
}