import React, { Component } from 'react';
import ReactKeymaster from 'react-keymaster';
import { GamePhases } from '../../../../actions/Constants/GamePhases';

import localStorage from 'store';

export default class EventPanel extends Component {
    constructor(props) {
        super(props);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    onKeyDown(keyName) {
        console.log(keyName);
        this.props.initNextPhase();
    }

    initNextPhase() {
        this.props.initNextPhase();
    }

    render() {

        const { gameData, gamePlay } = this.props;
        var eventDiv = '';
        var activityDesc = '';
        var playerName = '';
        var eventPanelClass = 'alert-info';

        var teamsList = localStorage.get('teamsList');

        console.log('evenPanel',gamePlay);

        switch(GamePhases[gameData.phase]){
            case 'End_Game' : {
                activityDesc = <span>Koniec zápasu !</span>;
            }break;
            default : {
                if (gamePlay.diceThrowResult !== false) {
                    switch(gamePlay.diceThrowResult.action){
                        case 'shoot' : activityDesc = <span>{gamePlay.diceThrowResult.result ? "GÓL !!! " : "Strela neúspešná"}</span>
                            playerName = <span>{gamePlay.diceThrowResult.activeShooter}</span>;
                            break;
                        case 'assistance' : activityDesc = <span>{gamePlay.diceThrowResult.result ? "Asistencia úspešná ! "/* + " +" + gamePlay.diceThrowResult.assistanceBonus*/ : "Asistencia neúspešná"}</span>
                            playerName = <span>{gamePlay.diceThrowResult.activeAssister}</span>;
                            break;
                        case 'buly' : activityDesc = <span>{"Hod kockou : " + gamePlay.diceThrowResult.diceResult.sum}</span>
                            playerName = <span>{teamsList[gameData.activeTeam].code}</span>;
                            break;
                        case 'fight' : activityDesc = <span>{"Hod kockami (2) : " + gamePlay.diceThrowResult.diceResult.sum + " + "+ gamePlay.diceThrowResult.attacker_attr_value + " = " + gamePlay.diceThrowResult.resultAbsolute}</span>
                            playerName = <span>{teamsList[gameData.activeTeam].code}</span>;
                            break;
                    }

                    if(gamePlay.diceThrowResult.action!='buly' && gamePlay.diceThrowResult.action!='fight') {
                        switch (gamePlay.diceThrowResult.result) {
                            case true :
                                eventPanelClass = 'alert-success';
                                break;
                            case false :
                                eventPanelClass = 'alert-danger';
                                break;
                        }
                    }
                }
            }
        }

        if(activityDesc!='') {
            eventDiv =
                <div id="eventPanelResult">
                    {activityDesc}
                    <ReactKeymaster
                        keyName="enter"
                        onKeyDown={this.onKeyDown}
                        />
                </div>;
        }

        return(
            <div id="eventPanel" onClick={()=>this.initNextPhase()} className={eventPanelClass}>
                <div id="eventPanelPlayerName">{playerName}</div>
                {eventDiv}
            </div>
        )
    }
}