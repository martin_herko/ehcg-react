import React, { Component} from 'react';

export default class InGameMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div id="InGameMenu">
                <div className="col-xs-12 text-right">
                    <h3><span className="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>Menu</h3>
                </div>
            </div>
        )
    }
}