import React, { Component} from 'react';
import { HomeGoaltender,HomeDefenseman1,HomeDefenseman2,HomeLeftWing,HomeRightWing,HomeCenter,
    AwayGoaltender,AwayDefenseman1,AwayDefenseman2,AwayLeftWing,AwayRightWing,AwayCenter} from './Players/Players';

export default class GameField extends Component {
    constructor(props) {
        super(props);
    }

    onPlayerCardClick(playerData,action) {
        this.props.onPlayerCardClick(playerData,action);
    }

    render() {

        const { gameData } = this.props;
        console.log('GameField gameData',gameData);

        var Roster = {};
        for(var i=0;i<=5;i++)
        {
            if(typeof gameData.homePlayerRoster != 'undefined') {
                Roster[i] = typeof gameData.homePlayerRoster[gameData.period][i] != 'undefined' ? gameData.homePlayerRoster[gameData.period][i] : getDefaultPlayerData();
            }
            else {
                Roster[i] = getDefaultPlayerData();
            }

            Roster[i]['index'] = i;
        }
        for(var i=0;i<=5;i++)
        {
            if(typeof gameData.awayPlayerRoster != 'undefined') {
                Roster[i+6] = typeof gameData.awayPlayerRoster[gameData.period][i] != 'undefined' ? gameData.awayPlayerRoster[gameData.period][i] : getDefaultPlayerData();
            }
            else {
                Roster[i+6] = getDefaultPlayerData();
            }

            Roster[i+6]['index'] = i;
        }

        console.log(Roster);

        switch(gameData.period){
            case 3 :
            case 4 :
            case 5 : {
                if(gameData.action!='buly') {
                    switch (gameData.ppTeam1) {
                        case gameData.homePlayer :
                            return renderOvertime1(gameData, Roster, this);
                        case gameData.awayPlayer :
                            return renderOvertime2(gameData, Roster, this);
                    }
                }
            }
            default : {
                return renderStandard(gameData,Roster, this);
            }
        }
    }
}

function renderStandard(gameData, Roster, parentComponent){
    /*let playerIndexes = [5,0,1,2,3,4,11,6,7,8,9,10];
    var players = [];

    for(var index in playerIndexes){
        players.push();
    }*/

    return (<div id="players">
        <HomeGoaltender gameData={gameData} playerData={Roster[5]} img={6} />
        <HomeDefenseman1 gameData={gameData} playerData={Roster[0]} img={2} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[0],action)}/>
        <HomeDefenseman2 gameData={gameData} playerData={Roster[1]} img={3} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[1],action)}/>
        <HomeLeftWing gameData={gameData} playerData={Roster[2]} img={4} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[2],action)}/>
        <HomeRightWing gameData={gameData} playerData={Roster[3]} img={5} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[3],action)}/>
        <HomeCenter gameData={gameData} playerData={Roster[4]} img={1} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[4],action)}/>
        <AwayGoaltender gameData={gameData} playerData={Roster[11]} img={32} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[11],action)}/>
        <AwayDefenseman1 gameData={gameData} playerData={Roster[6]} img={8} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[6],action)}/>
        <AwayDefenseman2 gameData={gameData} playerData={Roster[7]} img={9} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[7],action)}/>
        <AwayLeftWing gameData={gameData} playerData={Roster[8]} img={10} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[8],action)}/>
        <AwayRightWing gameData={gameData} playerData={Roster[9]} img={11} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[9],action)}/>
        <AwayCenter gameData={gameData} playerData={Roster[10]} img={7} onPlayerCardClick={(action)=>parentComponent.onPlayerCardClick(Roster[10],action)}/>
    </div>)
}

function renderOvertime1(gameData,Roster){
    return (<div id="players">
        <HomeGoaltender gameData={gameData} playerData={Roster[5]} img={6} />
        <HomeLeftWing gameData={gameData} playerData={Roster[2]} img={4} />
        <HomeRightWing gameData={gameData} playerData={Roster[3]} img={5} />
        <HomeCenter gameData={gameData} playerData={Roster[4]} img={1} />
        <AwayGoaltender gameData={gameData} playerData={Roster[11]} img={32} />
        <AwayDefenseman1 gameData={gameData} playerData={Roster[6]} img={8} />
        <AwayDefenseman2 gameData={gameData} playerData={Roster[7]} img={9} />
    </div>)
}

function renderOvertime2(gameData,Roster){
    return (<div id="players">
        <HomeGoaltender gameData={gameData} playerData={Roster[5]} img={6} />
        <HomeDefenseman1 gameData={gameData} playerData={Roster[0]} img={2} />
        <HomeDefenseman2 gameData={gameData} playerData={Roster[1]} img={3} />
        <AwayGoaltender gameData={gameData} playerData={Roster[11]} img={32} />
        <AwayLeftWing gameData={gameData} playerData={Roster[8]} img={10} />
        <AwayRightWing gameData={gameData} playerData={Roster[9]} img={11} />
        <AwayCenter gameData={gameData} playerData={Roster[10]} img={7} />
    </div>)
}

function getDefaultPlayerData() {
    return {
        is_shooting:false,
        is_assisting:false,
        dice_shoot_result:false,
        dice_assist_result:false,
        shoot_result:false,
        assist_result:false,
        atr1:0,
        atr2:0,
        atr3:0,
        experience:0,
        id:0,
        index:0,
        team_id:0,
        injuries:0,
        name:"Načítava sa ...",
        position:"",
        pot1:0,
        pot2:0,
        pot3:0,
        value:0
    }
}