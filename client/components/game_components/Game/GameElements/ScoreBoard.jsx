import React, { Component} from 'react';
/*import ReactCSSTransitionGroup from 'react-addons-css-transition-group';*/
/*</ReactCSSTransitionGroup>*/
/*<ReactCSSTransitionGroup transitionAppear={true} transitionName="example" transitionEnterTimeout={500} transitionLeaveTimeout={500}>*/

import localStorage from 'store';

export default class ScoreBoard extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const { gameData } = this.props;
        var teamsList = localStorage.get('teamsList');
        console.log('ScoreBoard - teamsList',teamsList);
        console.log('ScoreBoard - gameData', gameData);

        return(
            <div className="row" id="scoreBoard">
                <div className="col-xs-1" id="phase">{ gameData.period }. per<br/>PHA { gameData.phase }</div>
                <div className="col-xs-1">
                        <div className="pull-left text-right">
                                <div key="test0">
                                    <h3 key="test1">HOME</h3>
                                </div>
                        </div>
                </div>
                <div className="col-xs-3 text-right" id="homeTeamLogo">
                    <img className="img responsive" src={'/images/teams/' + teamsList[gameData.homePlayer].code + '.gif'}/></div>
                <div className="col-xs-1 text-right" id="homeTeamScore">
                    <h3>{ gameData.homeScore }</h3>
                </div>
                <div className="col-xs-1 text-center" id="scoreDelimiter"><h3> : </h3></div>
                <div className="col-xs-1 text-left" id="awayTeamScore">
                    <h3>{ gameData.awayScore }</h3>
                </div>
                <div className="col-xs-3 text-left" id="awayTeamLogo">
                    <img className="img responsive" src={'/images/teams/' + teamsList[gameData.awayPlayer].code + '.gif'}/>
                </div>
                <div className="col-xs-1">
                    <div className="pull-right text-left"><h3>AWAY</h3></div>
                </div>
            </div>
        )
    }
}