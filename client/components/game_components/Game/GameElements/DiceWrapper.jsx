import React, { Component} from 'react';
import Dice from './Dice';

export default class DiceWrapper extends Component {
    constructor(props) {
        super(props);
    }

    initDiceThrow() {
        this.props.onInitDiceThrow();
    }

    render() {
        const { gameData, gamePlay } = this.props;

        return(
            <Dice gamePlay={gamePlay} onInitDiceThrow={()=>this.initDiceThrow()}/>
        )
    }
}