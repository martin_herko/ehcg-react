import './GameWrapper.css';

import React, { Component} from 'react';

import ActionCards from './GameElements/ActionCards';
import Chat from './GameElements/Chat';
import DiceWrapper from './GameElements/DiceWrapper';
import EventPanel from './GameElements/EventPanel';
import GameField from './GameElements/GameField';
import InGameMenu from './GameElements/InGameMenu';
import InfoPanel from './GameElements/InfoPanel';
import ScoreBoard from './GameElements/ScoreBoard';

export default class GameWrapper extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { gameData } = this.props;
        this.props.onHomeTeamJoin(gameData.homePlayer);
        this.props.onAwayTeamJoin(gameData.awayPlayer);
    }

    componentDidUpdate() {
        const { gameData } = this.props;
        //Potom ako sa nacita roster, treba checknut ci su uz oba rostre nacitane a nasledne initGamePlay
        if(gameData.action=='gameplayInit' && typeof gameData.awayPlayerRoster !== 'undefined' && typeof  gameData.homePlayerRoster !== 'undefined') {
            this.props.initGamePlay();
        }
    }

    initDiceThrow() {
        this.props.onThrowDice();
    }

    initNextPhase() {
        this.props.initNextPhase();
    }

    onPlayerCardClick(playerData,action) {
        this.props.onPlayerCardClick(playerData,action);
    }

    render() {
        const { gameData, gamePlay } = this.props;
        console.log('GameWrapper gameData',gameData);

        return (
            <div className="container">
                <div id="gameWrapper">
                    <div className="row" id="gameHeader">
                        <div className="col-xs-12 no-padding">
                            <div className="col-xs-10">
                                <ScoreBoard gameData={gameData} />
                            </div>
                            <div className="col-xs-2" id="inGameMenu">
                                <InGameMenu />
                            </div>
                        </div>
                    </div>
                    <div className="row" id="gameBody">
                        <div id="gameField" className="row">
                            <GameField gameData={gameData} onPlayerCardClick={(playerData,action)=>this.onPlayerCardClick(playerData,action)}/>
                            <div id="gameDialogs">
                            </div>
                        </div>
                        <div className="row" id="diceWrapper">
                            <DiceWrapper gamePlay={gamePlay} onInitDiceThrow={()=>this.initDiceThrow()}/>
                        </div>
                        {<div className="row" id="eventPanelWrapper">
                            <EventPanel gameData={gameData} gamePlay={gamePlay} initNextPhase={()=>this.initNextPhase()}/>
                        </div>}
                    </div>
                    <div className="row" id="gameFooter">
                        <div className="col-xs-8">
                            <ActionCards/>
                        </div>
                        <div className="col-xs-4">
                            <Chat/>
                        </div>
                        <div className="col-xs-12">
                            <InfoPanel gameData={gameData} gamePlay={gamePlay}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}