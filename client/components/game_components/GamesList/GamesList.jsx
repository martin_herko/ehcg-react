import React, { Component} from 'react';
import { fetchGamesList, reloadingGamesListPage } from '../../../actions/GameSelectActions';
import localStorage from 'store';

export default class GamesList extends Component {
    constructor(props) {
        super(props);
        console.log('GamesList props constructor', this.props);
    }

    gameSelected(game) {
        this.props.onGameSelected(game);
    }

    gameLaunched(game) {
        this.props.onGameLaunch(game);
    }

    setRosters(game) {
        this.props.onSetRosters(game);
    }

    render() {
        var GameList;
        var gameObjects = this.props.gamesListData;
        var selectedTeam = this.props.selectedTeam;
        var isGameJoined = this.props.isGameJoined;
        var isHomePlayerJoined = this.props.isHomePlayerJoined;
        var isAwayPlayerJoined = this.props.isAwayPlayerJoined;
        var teamsList = localStorage.get('teamsList');
        console.log('teamsList',teamsList);
        console.log('gamelist props', this.props);
        console.log('gameObjects', gameObjects);
        console.log('isGameJoined', isGameJoined);
        var _this = this;

        if(isGameJoined) {
            if(isHomePlayerJoined && isAwayPlayerJoined){
                GameList = <div>
                    {/* <button onClick={() => _this.gameLaunched(gameObjects[0])}>Spustiť hru</button><br/> */}
                    <button onClick={() => _this.setRosters(gameObjects[0])}>Zostav linie</button>
                </div>;
            }else if (isHomePlayerJoined && !isAwayPlayerJoined){
                GameList = <div>Čaká sa na tím hostí</div>;
            }else if (!isHomePlayerJoined && isAwayPlayerJoined) {
                GameList = <div>Čaká sa na domáci tím</div>;
            }
        }
        else {
            if (Object.keys(gameObjects).length === 0) {
                GameList = <p> Neboli nájdené žiadne aktívne hry. </p>;
            }
            else {
                GameList = [];

                Object.keys(gameObjects).forEach(function (game,value) {
                    //if game.home_team_connected && game.away_team!=this.props.selectedTeam
                    //inak povedane, nemoze sa pripojit ako host, ak domaci je rovnaky tim
                    //@todo - ako riesit vypadok spojenia? nieco ako socket.lose.blabla tak update na tabulke team_connected
                    let gameData = gameObjects[game];
                    let homeTeamLabel = gameData.home_team>0 ? teamsList[gameData.home_team].code : '-';
                    let awayTeamLabel = gameData.away_team>0 ? teamsList[gameData.away_team].code : '-';

                    if(5==0 /*gameData.home_team_connected && (gameData.home_team==selectedTeam.id || gameData.away_team==selectedTeam.id)*/){
                        GameList.push(
                            <div key={game} className="col-xs-12"><span>{gameData.label} : {homeTeamLabel} vs {awayTeamLabel}</span></div>
                        );
                    } else{
                        GameList.push(
                            <div key={game} className="col-xs-12">
                                <a onClick={() => _this.gameSelected(game)}>
                                    <span>{gameData.label} : {homeTeamLabel} vs {awayTeamLabel}</span>
                                </a>
                            </div>);
                    }
                });

                GameList = <div className="row col-xs-12"> {GameList} </div>;
            }

            if(!selectedTeam) {
                GameList = <p> Pre zobrazenie zoznamu najskôr vyberte tím </p>;
            }
        }

        return (
            <div>
                <p>Vyberte hru</p>
                {GameList}
            </div>
        );
    }
}