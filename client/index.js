import 'babel-core/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root';

let rootElement = document.getElementById("body");

ReactDOM.render(
  <Root />,
  rootElement
);
