var socketio = require('socket.io');
var todoModel = require('../models/todos');
var teamModel = require('../models/teams.js');
var gameModel = require('../models/games.js');
var rostersModel = require('../models/rosters.js');
var io;

module.exports = {

    setServer: function (server) {
        io = socketio(server);

        io.on('connection', function (socket) {
            console.log('a user connected', socket.id);

            socket.on('viewing', function () {
                todoModel.getAllUniversalTodos(function (results) {
                    socket.emit("current-universal-todos", results);
                });
            });

            socket.on('disconnect', function () {
                console.log('user disconnected, socketID : ' + socket.id);
                //@todo - check if socketID is in any games, if so, than emit updated game status
            });

            socket.on('action', (action) => {
                console.log('action', action);
                switch (action.type) {
                    case 'server/hello' :
                        console.log('Got hello data!');
                        socket.emit('action', {type: 'message', data: 'good day!'});
                        break;
                    case 'server/viewing' :
                        todoModel.getAllUniversalTodos(function (results) {
                            socket.emit("current-universal-todos", results);
                        });
                        break;
                    case 'server/teamsList' :
                        teamModel.getAllTeams(
                            function (result) {
                                //console.log(result);
                                socket.emit('action', {type: 'teamsList', data: result.teams});
                            });
                        break;
                    case 'server/gamesList' :
                        gameModel.getAllGames(
                            function (result) {
                                //console.log(result);
                                socket.emit('action', {type: 'gamesList', data: result.games});
                            }
                        )
                        break;
                    case 'server/gameSelected' :
                        //join to room
                        socket.join(action.id);
                        var joinedPlayersCount = io.sockets.adapter.rooms[action.id].length;
                        //first client = home, second = away
                        gameModel.joinToGame(action.id, action.team, joinedPlayersCount, socket.id,
                            function () {
                                //io.sockets.in(action.id).emit('action', {type: 'message', data: 'hi guys in game ' + action.id + ' !' });
                                gameModel.getGameData(action.id,
                                    function (result) {
                                        //console.log('getGameDataResult',result);
                                        io.sockets.in(action.id).emit('action', {
                                            type: 'joinedToGame',
                                            data: {game: result.game, joinedPlayersCount: joinedPlayersCount}
                                        });
                                        //socket.emit('action', {type: 'joinedToGame', data : result.game});
                                    }
                                );
                            }
                        );

                        /*socket.emit('action', {type: 'joinedToGame', data : result.game});*/
                        break;
                    case 'server/teamRoster' :
                        console.log(action);
                        teamModel.getTeamRoster(action.teamId, action.gameId,
                            function (result) {
                                //console.log(result);
                                socket.emit('action', {type: 'teamRoster', data: result.roster, role: action.role});
                            }
                        );
                        break;

                    case 'server/teamRostersAvailPlayerList' :
                        console.log(action);
                        rostersModel.getTeamRostersAvailPlayerList(action.teamId, action.gameId, function (result) {
                            io.sockets.in(action.gameId).emit('action', {
                                type: 'teamPlayerList',
                                data: result.roster,
                                teamId: action.teamId
                            });
                        });
                        /*teamModel.getTeamPlayerList(action.id, function(result){
                         socket.emit('action', {type: 'teamPlayerList', data : result.roster, role:action.role});
                         });*/
                        break;
                    case 'server/assignPlayerPosition' :
                        console.log(action);
                        rostersModel.assignPlayerGamePosition(action.data, function (result) {
                            io.sockets.in(action.data.game_id).emit('action', {
                                type: 'assignPlayerPositionResult',
                                data: action.data,
                                result: result.result
                            });
                        });
                        break;
                    case 'server/removePlayerFromRosterPosition' :
                        console.log(action);
                        rostersModel.removePlayerFromGamePosition(action.data, function (result) {
                            io.sockets.in(action.data.game_id).emit('action', {
                                type: 'removePlayerFromRosterPosition',
                                data: action.data,
                                result: result.result
                            });
                        });
                        break;
                    case 'server/FetchGameRosters' :
                        console.log(action);
                        rostersModel.getGameRosters(action.gameId, function (result) {
                            io.sockets.in(action.gameId).emit('action', {
                                type: 'gameRostersUpdate',
                                data: {rosters: result.data}
                            });
                        });
                        break;
                    case 'server/requestGoaltenderDiceDraw' :
                        console.log(action);
                        rostersModel.doGoaltenderDiceDraw(action.gameId, action.teamId, function (result) {
                            var data = result.data;
                            data.game_id = action.gameId;
                            data.team_id = action.teamId;
                            io.sockets.in(action.gameId).emit('action', {
                                type: 'gameRostersGoaltenderDiceDrawResult',
                                result: result.result,
                                data: data
                            });
                        });
                        break;
                    case 'server/teamRosterCompleted' :
                        console.log(action);
                        rostersModel.getGameRosters(action.gameId, function (rostersResult) {
                            //Tu treba urobit check, ak true, tak updateGameState celkovo..
                            gameModel.setTeamRosterCompleted(action.gameId, action.teamId, action.role, rostersResult.data, function (result) {
                                if (result) {
                                    gameModel.getGameData(action.gameId, function (gameResult) {
                                        io.sockets.in(action.gameId).emit('action', {
                                            type: 'gameUpdate',
                                            data: gameResult.game
                                        })
                                    });
                                }
                            });
                        });
                        break;
                }
            });

        });
    },

    notifyAllListenersOfNewTodo: function (todo) {
        io.emit('new-todo', todo);
    }

}

/*
 io.sockets.emit
 - will send to all the clients
 socket.broadcast.emit
 - will send the message to all the other clients except the newly created connection
 */
