/**
 * Created by Gordon on 21.8.2017.
 */

var mysql = require('mysql');
var dbConnectionCreator = require('../utilities/mysqlConnection.js');

const TABLE_GAME_ROSTERS = 'game_rosters';

var rostersModel = {
    getGameRosters: function(gameId, callback) {
        var sql = constructGetGameRosters(gameId);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results) {
            dbConnection.destroy();
            if (error) {
                console.log(error);
                return callback({error: error});
            } else {
                var rosters = results;
                //console.log(results);
                return callback({data: rosters});
            }
        });

    },
    getTeamRostersAvailPlayerList : function(teamId, gameId, callback) {
        var dbConnection = dbConnectionCreator();
        var sqlString = constructGetTeamRostersAvailPlayerListQuery(teamId, gameId);
        dbConnection.query(sqlString, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else {
                return callback({roster: results});
            }
        });
    },
    assignPlayerGamePosition: function(data, callback) {
        var sql = constructSetPlayerGamePositionQuery(data);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results) {
            dbConnection.destroy();
            if (error) {
                console.log(error);
                return callback({error: error, result:false});
            } else {
                callback({result:true});
            }
        });
    },
    removePlayerFromGamePosition: function(data, callback) {
        var sql = constructRemovePlayerFromGamePositionQuery(data.gameRostersId);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results) {
            dbConnection.destroy();
            if (error) {
                console.log(error);
                return callback({error: error, result:false});
            } else {
                callback({result:true});
            }
        });
    },
    doGoaltenderDiceDraw: function(gameId, teamId, callback) {
        //1-6
        var dice_result = Math.floor( Math.random() * ( 1 + 6 - 1 ) ) + 1;
        var gTeamPosPriority = dice_result<=3? 1 : 2;

        var sql = constructSetGoaltenderByDiceDraw(gameId, teamId, gTeamPosPriority);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results) {
            dbConnection.destroy();
            if (error) {
                console.log(error);
                return callback({error: error, result:false});
            } else {
                if(results[0].player_id!=='undefined') {
                    rostersModel.saveGoaltenderDiceDrawResult(gameId, teamId, results[0].player_id);
                }
                return callback({data:{dice_result:dice_result, player_id:results[0].player_id}, result:true});
            }
        });
    },
    saveGoaltenderDiceDrawResult : function(gameId, teamId, player_id) {
        var sql = constructSaveGoaltenderDiceDrawResults(gameId, teamId, player_id);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results) {
            dbConnection.destroy();
            if (error) {
                console.log(error);
            } else {
                console.log('saveGoaltenderDiceDrawResult : true');
            }
        });
    }
}

function constructSetPlayerGamePositionQuery(data) {
    var game_id = parseInt(mysql.escape(data.game_id));
    var team_id = parseInt(mysql.escape(data.team_id));
    var period = parseInt(mysql.escape(data.period));
    var player_id = parseInt(mysql.escape(data.player_id));
    var position = mysql.escape(data.position);

    var query = "INSERT INTO " + TABLE_GAME_ROSTERS + " VALUES('',"+game_id+","+team_id+","+period+","+player_id+","+position+") " +
        "ON DUPLICATE KEY UPDATE player_id="+player_id+",position="+position+";";

    console.log(query);
    return query;
}

function constructRemovePlayerFromGamePositionQuery(gameRostersId) {
    var gameRostersId = parseInt(mysql.escape(gameRostersId));

    var query = "DELETE FROM "+TABLE_GAME_ROSTERS+" WHERE id="+gameRostersId;
    console.log(query);
    return query;
}

function constructGetTeamRostersAvailPlayerListQuery(teamId, gameId) {
    var teamId = mysql.escape(teamId);
    var gameId = mysql.escape(gameId);

    var query = 'SELECT p.*, pst.team_id, pst.team_position_priority, pa.* '
        + 'FROM player p JOIN player_season_team pst ON p.id=pst.player_id '
        + 'LEFT JOIN player_atr pa ON p.id=pa.player_id '
        + 'LEFT JOIN gameRostersView grw ON p.id=grw.player_id AND grw.game_id='+gameId
        + ' WHERE pst.team_id='+teamId
        + ' AND grw.player_id IS NULL '
        + 'GROUP BY p.id ORDER BY p.position DESC';

    console.log(query);
    return query;
}

function constructGetGameRosters(gameId) {
    var query = "SELECT * FROM gameRostersView WHERE game_id=" + parseInt(mysql.escape(gameId));
    console.log(query);
    return query;
}

function constructSetGoaltenderByDiceDraw(gameId, teamId, gTeamPosPriority) {
    var gameId = mysql.escape(gameId);
    var teamId = mysql.escape(teamId);
    var gTeamPosPriority = mysql.escape(gTeamPosPriority);

    var query = "SELECT pst.player_id "
        +"FROM player_season_team pst "
        +"JOIN games g ON pst.season_id=g.season AND pst.season_id=g.season "
        +"JOIN player p ON pst.player_id=p.id "
        +"WHERE g.id="+gameId+" AND pst.team_id="+teamId+" AND pst.team_position_priority="+gTeamPosPriority+" AND p.position='g'";

    console.log(query);
    return query;
}

function constructSaveGoaltenderDiceDrawResults(game_id, team_id, player_id) {
    var query = "INSERT INTO " + TABLE_GAME_ROSTERS + " (id,game_id,team_id,period,player_id,position) "+
        "VALUES ('',"+game_id+","+team_id+",1,"+player_id+",'g'), " +
        "('',"+game_id+","+team_id+",2,"+player_id+",'g'), " +
        "('',"+game_id+","+team_id+",3,"+player_id+",'g'), " +
        "('',"+game_id+","+team_id+",4,"+player_id+",'g'), " +
        "('',"+game_id+","+team_id+",5,"+player_id+",'g') " +
        "ON DUPLICATE KEY UPDATE player_id=VALUES(player_id);";

    console.log(query);
    return query;
}

module.exports = rostersModel;
