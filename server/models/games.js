var mysql = require('mysql');
var dbConnectionCreator = require('../utilities/mysqlConnection.js');

const TABLE_GAMES = 'games';

var gameModel = {
    getAllGames: function (callback) {
        var sql = constructGetAllGamesQuery();
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else {
                var games = {};
                results.forEach(function (result) {
                    games[result.id] = result;
                });
                return callback({games: games});
            }
        });
    },
    createGame: function (label, callback) {
        var sql = constructCreateGamesSql(label);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results, fields) {
            if (error) {
                dbConnection.destroy();
                // return res.json({error: error, when: "inserting"});
                return (callback({error: error, when: "inserting"}));
            } else {
                var getGameSqlString = constructGetGameSql(results.insertId);
                dbConnection.query(getGameSqlString, function (error, results, fields) {
                    dbConnection.destroy();
                    if (error) {
                        return res.json({error: error, when: "reading"});
                        return (callback({error: error, when: "reading"}));
                    } else {
                        return callback({game: results});
                    }
                });
            }
        });
    },
    joinToGame: function (id, team, playerNumber, socketId, callback) {
        var sql = constructJoinGamesSql(id, team, playerNumber, socketId);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else{
                return callback();
            }
        });
    },
    setTeamRosterCompleted: function (id, teamId, role, rosters, callback){
        var isTeamRosterCompleted = checkIfTeamRosterIsCompleted(rosters,teamId);
        var sql = constructSetTeamRosterCompleted(id,role,isTeamRosterCompleted);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else{
                return callback({result : isTeamRosterCompleted});
            }
        });
    },
    getGameData: function (id, callback) {
        var sql = constructGetGameSql(id);
        var dbConnection = dbConnectionCreator();
        dbConnection.query(sql, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else {
                var game = results[0];
                return callback({game});
            }
        });
    }
};

function constructGetAllGamesQuery() {
    var query = " SELECT * FROM " + TABLE_GAMES;
    return query;
}

function constructGetGameSql(id) {
    var query = " SELECT * FROM " + TABLE_GAMES + " WHERE id=" + mysql.escape(id);
    return query;
}

function constructCreateGamesSql(label) {
    var query = "INSERT INTO " + TABLE_GAMES + " SET " +
            " label = " + mysql.escape(label);
    return query;
}

function constructJoinGamesSql(id, team, playerNumber, socketId) {
    var position = playerNumber == 1 ? 'home_team' : 'away_team';
    var connected = position+'_connected';
    var socket = position+'_socket_id';

    var query = "UPDATE " + TABLE_GAMES + " SET " +
            position + " = " + mysql.escape(team) + "," +
            connected + " = 1, " +
            socket + " = " + mysql.escape(socketId) +
            " WHERE " +
            " id = " + mysql.escape(id);
    return query;
}

function checkIfTeamRosterIsCompleted(rosters,teamId){
    var roster = {1: {}, 2: {}, 3: {}, 4: {}, 5: {}};
    for(var key in rosters){
        var playerTeamId = rosters[key]['team_id'];
        if(teamId == playerTeamId) {
            var period = rosters[key].period;
            var position = rosters[key].position;
            roster[period][position] = rosters[key];
        }
    }

    var linesToCheck = [1,2];
    var positionsToCheck = ['d1','d2','lw','c','rw','g'];

    for(var l in linesToCheck){
        if(typeof(roster[linesToCheck[l]]) ==='undefined'){
            return false;
        }
        for(var p in positionsToCheck){
            if(typeof(roster[linesToCheck[l]][positionsToCheck[p]]) ==='undefined'){
                return false;
            }
        }
    }

    return true;
}

function constructSetTeamRosterCompleted(gameId,role,isTeamRosterCompleted){
    var role = role == 'home' ? 'home_team_roster_completed':'away_team_roster_completed';
    var completed = isTeamRosterCompleted ? 1:0;

    var query = "UPDATE " + TABLE_GAMES + " SET " +
        role + " = " + mysql.escape(completed) +
        " WHERE" +
        " id = " + mysql.escape(gameId);
    console.log(query);

    return query;
}

module.exports = gameModel;
