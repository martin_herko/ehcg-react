/**
 * Sample model
 */
var mysql = require('mysql');
var dbConnectionCreator = require('../utilities/mysqlConnection.js');

const TABLE_TEAMS = 'teams';

var teamModel = {
    getAllTeams: function (callback) {
        var dbConnection = dbConnectionCreator();
        var sqlString = constructGetAllTeamsSqlString();
        dbConnection.query(sqlString, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else {
                var teams = results;
                return callback({teams: teams});
            }
        });
    },
    getTeamDetails : function (teamId, callback) {
        var dbConnection = dbConnectionCreator();
        var sqlString = constructGetTeamDetailsSqlString(teamId);
        dbConnection.query(sqlString, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else {
                var teams = results;
                return callback({teams: teams});
            }
        });
    },
    getTeamRoster : function (teamId, gameId, callback) {
        var dbConnection = dbConnectionCreator();
        var sqlString = constructGetTeamRosterSql(teamId,gameId);
        dbConnection.query(sqlString, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else {
                var roster = generateTeamRosterLines(results);
                return callback({roster: roster});
            }
        });
    },
    getTeamPlayerList : function(teamId, callback) {
        var dbConnection = dbConnectionCreator();
        var sqlString = constructGetTeamPlayerListSql(teamId);
        dbConnection.query(sqlString, function (error, results, fields) {
            dbConnection.destroy();
            if (error) {
                return callback({error: error});
            } else {
                var roster = generateTeamPlayerList(results);
                return callback({roster: roster});
            }
        });
    }
};

function constructGetAllTeamsSqlString() {
    var query = " SELECT * FROM " + TABLE_TEAMS + " WHERE active=1" ;
    return query;
}

function constructGetTeamDetailsSqlString(teamId) {
    var query = " SELECT * FROM " + TABLE_TEAMS + " WHERE id= " + mysql.escape(teamId) ;
    return query;
}

function constructGetTeamRosterSqlUniversal(teamId) {
    teamId = mysql.escape(teamId);

    var query = '(SELECT p.*, pst.team_id, SUM(p.atr1 + p.atr2 + p.atr3) as experience,pa.* '
        + 'FROM player p JOIN player_season_team pst ON p.id=pst.player_id '
        + 'LEFT JOIN player_atr pa ON p.id=pa.player_id '
        + 'WHERE pst.team_id='+teamId+' AND FIND_IN_SET("d",p.position) GROUP BY p.id ORDER BY experience DESC LIMIT 0,4) '
        + 'UNION '
        + '(SELECT p.*, pst.team_id, SUM(p.atr1 + p.atr2 + p.atr3) as experience,pa.* '
        + 'FROM player p JOIN player_season_team pst ON p.id=pst.player_id '
        + 'LEFT JOIN player_atr pa ON p.id=pa.player_id '
        + 'WHERE pst.team_id='+teamId+' AND p.position REGEXP ("c|lw|rw") GROUP BY p.id ORDER BY experience DESC LIMIT 0,6) '
        + 'UNION '
        + '(SELECT p.*, pst.team_id, SUM(p.atr1 + p.atr2 + p.atr3) as experience,pa.* '
        + 'FROM player p JOIN player_season_team pst ON p.id=pst.player_id '
        + 'LEFT JOIN player_atr pa ON p.id=pa.player_id '
        + 'WHERE pst.team_id='+teamId+' AND FIND_IN_SET("g",p.position) GROUP BY p.id ORDER BY experience DESC LIMIT 0,2)';

    console.log(query);
    return query;
}

function generateTeamRosterLinesUniversal(players) {
    var roster = {
        // 0-d1, 1-d2, 2-lw, 3-rw, 4-c, 5-g
        1 : {0:players[0],1:players[1],2:players[4],3:players[5],4:players[6],5:players[10]},
        2 : {0:players[2],1:players[3],2:players[7],3:players[8],4:players[9],5:players[11]},
        3 : {0:players[0],1:players[1],2:players[4],3:players[5],4:players[6],5:players[10]},
        4 : {0:players[0],1:players[1],2:players[4],3:players[5],4:players[6],5:players[10]},
        5 : {0:players[0],1:players[1],2:players[4],3:players[5],4:players[6],5:players[10]}
    };
    return roster;
}

function constructGetTeamRosterSql(teamId,gameId) {
    teamId = parseInt(mysql.escape(teamId));
    gameId = parseInt(mysql.escape(gameId));

    var query = 'SELECT gr.position as game_position, gr.period as game_period, gr.team_id,'
    + 'SUM(p.atr1 + p.atr2 + p.atr3) as experience,'
    + 'pa.plus_minus,pa.ppg,pa.ppa,pa.ppp,pa.shg,pa.sha,pa.shp,pa.enf,pa.fight,pa.buly,pa.stats,'
    + 'p.* '
    + 'FROM game_rosters gr '
    + 'JOIN player p ON gr.player_id=p.id '
    + 'LEFT JOIN player_atr pa ON p.id=pa.player_id '
    + 'WHERE gr.team_id='+teamId+' AND gr.game_id='+gameId+' '
    + 'GROUP BY gr.player_id, gr.period '
    + 'ORDER BY gr.period, gr.position';
    console.log(query);
    return query;
}

function generateTeamRosterLines(players) {
    var roster = {
        // 0-d1, 1-d2, 2-lw, 3-rw, 4-c, 5-g
        1 : {0:{},1:{},2:{},3:{},4:{},5:{}},
        2 : {0:{},1:{},2:{},3:{},4:{},5:{}},
        3 : {0:{},1:{},2:{},3:{},4:{},5:{}},
        4 : {0:{},1:{},2:{},3:{},4:{},5:{}},
        5 : {0:{},1:{},2:{},3:{},4:{},5:{}}
    };

    for(var key in players){
        var player = players[key];
        switch(player.game_position){
            case 'd1' : roster[player.game_period][0] = player;
                break;
            case 'd2' : roster[player.game_period][1] = player;
                break;
            case 'lw' : roster[player.game_period][2] = player;
                break;
            case 'rw' : roster[player.game_period][3] = player;
                break;
            case 'c' : roster[player.game_period][4] = player;
                break;
            case 'g' : roster[player.game_period][5] = player;
                break;
        }
    }

    //temporaly fix
    roster[3] = roster[1];
    roster[4] = roster[1];
    roster[5] = roster[1];

    console.log(players);
    return roster;
}

function constructGetTeamPlayerListSql(teamId) {
    teamId = mysql.escape(teamId);

    var query = 'SELECT p.*, pst.team_id, pa.* '
    + 'FROM player p JOIN player_season_team pst ON p.id=pst.player_id '
    + 'LEFT JOIN player_atr pa ON p.id=pa.player_id '
    + 'WHERE pst.team_id='+teamId+' '
    + 'GROUP BY p.id ORDER BY p.position DESC';

    console.log(query);
    return query;
}

function generateTeamPlayerList(players) {
    return players;
}

module.exports = teamModel;