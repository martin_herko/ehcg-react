var authenticationMiddleware = require('../middlewares/authentication.js'); //todo apply when needed
var teamModel = require('../models/teams.js');

var setTeamRoutes = function (router) {

    router.get('/api/v1/getTeams',
        function (req, res) {
            var userId = req.params.id;
            teamModel.getAllTeams(
                function (result) {
                    return res.json(result);
                }
            );
        }
    );

}

module.exports = setTeamRoutes;
